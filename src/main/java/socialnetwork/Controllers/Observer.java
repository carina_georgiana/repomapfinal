package socialnetwork.Controllers;


public interface Observer {
    void update();
}