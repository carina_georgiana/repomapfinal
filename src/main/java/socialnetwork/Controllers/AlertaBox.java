package socialnetwork.Controllers;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
public class AlertaBox {
    /**
     *
     * @param title-titlu fereastra
     * @param mesaj-mesajul din fereastra de pop-up
     */
    public static void display(String title,String mesaj){
        Stage window2=new Stage();//a 2 a fereastra
        window2.initModality(Modality.APPLICATION_MODAL);
        window2.setTitle(title);
        window2.setMinHeight(250);
        window2.setMinWidth(250);

        Label label=new Label();
        label.setText(mesaj);
        label.styleProperty().set("-fx-font-weight: bold italic");
        label.styleProperty().set("-fx-font-size: 1.3em");
        Button closebtn=new Button("exit");
        closebtn.setOnAction(e->window2.close());

        VBox layout=new VBox(10);
        layout.getChildren().addAll(label,closebtn);
        layout.setAlignment(Pos.CENTER);
        Scene scene=new Scene(layout);
        scene.getStylesheets().add("/StyleMain.css");

        window2.setScene(scene);
        window2.showAndWait();//nu pot face nimic in alte ferestre


    }
}
