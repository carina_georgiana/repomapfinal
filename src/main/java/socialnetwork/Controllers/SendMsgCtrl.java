package socialnetwork.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.EventService;
import socialnetwork.ui.UI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SendMsgCtrl {
    UI ui;
    Utilizator curent;
    EventService eventService;
    @FXML
    Pagination paginator;
    @FXML
    TableView<Utilizator> tableUsers;
    @FXML
    TableColumn<Utilizator,String>tableColumnfirstName;
    @FXML
    TableColumn<Utilizator,String>tableColumnlastName;
    @FXML
    TableColumn<Utilizator,String>tableColumnEmail;
    @FXML
    Label l_allusers;
    @FXML
    Label l_mesaj;
    @FXML
    Label l_useri;
    @FXML
    ListView<Utilizator>list_users;
    @FXML
    Button btn_search;
    @FXML
    Button btn_send;
    @FXML
    Button btn_back;
    @FXML
    TextField txt_search;
    @FXML
    TextField txt_mesaj;
    int flag=0;
    ObservableList<Utilizator> useri= FXCollections.observableArrayList();
    ObservableList<Utilizator> useriTabel= FXCollections.observableArrayList();

    public void handleAddSend(){
        Utilizator selected=tableUsers.getSelectionModel().getSelectedItem();
        List<Utilizator> list=new ArrayList<>();
        useri.forEach(x->list.add(x));
        list.add(selected);
        useri.setAll(list);
        list_users.getItems().setAll(list);
    }
    public void handleBack(ActionEvent event)throws IOException{
        //load pag de mesaje
        FXMLLoader loginParent=new FXMLLoader();
        loginParent.setLocation(getClass().getResource("/Messages2.fxml"));
        AnchorPane loginLayout=(AnchorPane)loginParent.load();
        Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(new Scene(loginLayout));

        MessagesCtrl2 ctrl=loginParent.getController();
        ctrl.ui=ui;
        ctrl.eventService=eventService;
        ctrl.curent=curent;
        ctrl.flag=flag;
        ctrl.init();
        window.show();

    }

    public void handleSend(){
        //iau lista useri la care trimite curent,formez mesaj si trimit
        if(txt_mesaj.getText().isEmpty()||useri.isEmpty()){
            AlertaBox.display("error","mesaj sau lista useri null");
            return;
        }
        List<Utilizator>toUseri=new ArrayList<>();
        useri.forEach(x->toUseri.add(x));
        List<Long>to=new ArrayList<>();
        toUseri.forEach(x->to.add(x.getId()));
        Long from=curent.getId();
        String mesaj=txt_mesaj.getText();
        Message nou=new Message(from,to,mesaj);
        ui.messageService.save(nou);
        useri.clear();
        list_users.getItems().clear();
        txt_mesaj.clear();
        AlertaBox.display("succes","mesaj trimis");
    }

    public void handleSearch(){
        if(txt_search.getText().isEmpty()){
            AlertaBox.display("atentie","va rugam introduceti text");
            return;
        }

        String email=txt_search.getText();
        List<Utilizator>cautati=StreamSupport.stream(ui.utilizatorService.getSearch(email).spliterator(),false)
                .collect(Collectors.toList());
        txt_search.clear();
        useriTabel.setAll(cautati);

        tableColumnfirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableColumnlastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        tableUsers.setItems(useriTabel);

        if(cautati.size()%5==0)
            paginator.setPageCount(cautati.size()/5);
        else
            paginator.setPageCount(cautati.size()/5+1);
        paginator.setPageFactory(this::createPage);
        useriTabel.setAll(cautati);
    }

    public void init(){
        //tabelul cu toti userii
        List<Utilizator> useri2=StreamSupport.stream(ui.utilizatorService.getAll().spliterator(),false)
                .collect(Collectors.toList());
        useriTabel.setAll(useri2);
        tableColumnfirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableColumnlastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        tableUsers.setItems(useriTabel);
        paginator.setMaxPageIndicatorCount(1);

        if(ui.utilizatorService.getSize()<=5)
            paginator.setPageCount(1);
        else{
            if(ui.utilizatorService.getSize()%5==0)
                paginator.setPageCount(ui.utilizatorService.getSize()/5);
            else
                paginator.setPageCount(ui.utilizatorService.getSize()/5+1);
    }
        paginator.setPageFactory(this::createPage);
    }
    public Node createPage(int pageIndex){
        int fromIndex=pageIndex*5;
        int toIndex=Math.min(fromIndex+5,useriTabel.size());
        tableUsers.setItems(FXCollections.observableArrayList(useriTabel.subList(fromIndex,toIndex)));
        return tableUsers;
    }
}
