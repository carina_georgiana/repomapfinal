package socialnetwork.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.EventService;
import socialnetwork.ui.UI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessagesCtrl2 {
    UI ui;
    Utilizator curent;
    EventService eventService;
    @FXML
    Pagination paginator;
    @FXML
    Label l_nefolosit;
    @FXML
    Label l_msg;
    @FXML
    Label l_messages;
    @FXML
    Label l_conv;
    @FXML
    Button btn_search;
    @FXML
    Button btn_reply;
    @FXML
    Button btn_send;
    @FXML
    Button btn_back;
    @FXML
    TextField txt_mesaj;
    @FXML
    TextField txt_search;
    @FXML
    TableView<Utilizator> tableUsers;
    @FXML
    TableColumn<Utilizator,String> tableColumnfirstName;
    @FXML
    TableColumn<Utilizator,String> tableColumnlastName;
    @FXML
    TableColumn<Utilizator,String> tableColumnEmail;
    @FXML
    ListView<Message>list_messages;
    int flag=0;
    ObservableList<Utilizator> useri= FXCollections.observableArrayList();

    public void handleBack(ActionEvent event){
        try{
            FXMLLoader loginParent=new FXMLLoader();
            loginParent.setLocation(getClass().getResource("/UserCurent.fxml"));
            AnchorPane loginLayout=(AnchorPane)loginParent.load();
            Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(new Scene(loginLayout));
            UserCurentCtrl loginCtrl=loginParent.getController();//seteaza controller pt fxml
            loginCtrl.ui=ui;
            loginCtrl.selectedUser=curent;
            loginCtrl.eventService=eventService;
            loginCtrl.flag=flag;
            loginCtrl.init();
            window.show();}catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param event-la apasare buton de send imi deschide fereastra de trimitere mesaj nou
     */
    public void handleSend(ActionEvent event){
        try{
            FXMLLoader loginParent=new FXMLLoader();
            loginParent.setLocation(getClass().getResource("/SendMessages.fxml"));
            AnchorPane loginLayout=(AnchorPane)loginParent.load();
            Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(new Scene(loginLayout));
            SendMsgCtrl ctrl=loginParent.getController();
            ctrl.ui=ui;
            ctrl.eventService=eventService;
            ctrl.curent=curent;
            ctrl.flag=flag;
            ctrl.init();
            window.show();}catch (IOException e){
            e.printStackTrace();
        }

    }

    /**
     * fct ce trimite mesaj de reply la mesajul selectat
     */
    public void handleReply(){
        Message selectedMsg=list_messages.getSelectionModel().getSelectedItem();
        if(selectedMsg!=null&&selectedMsg.getTo().contains(curent.getId())){
            if(txt_mesaj.getText().isEmpty()){
                //txt_mesaj.clear();
                AlertaBox.display("error","mesajul nu are text");
                return;
            }
            String mesaj=txt_mesaj.getText();
            Long id_to=selectedMsg.getId_from();
            List<Long>ids_to=new ArrayList<Long>();
            ids_to.add(id_to);
            Long id_from=curent.getId();

            Message nou=new Message(id_from,ids_to,mesaj,selectedMsg);
            ui.messageService.save(nou);
            //reload listview

            Utilizator selected=ui.utilizatorService.findId(selectedMsg.getId_from());
            Iterable<Message>msgs=ui.messageService.getAll();
            Predicate<Message> filtru1= x->{return (x.getId_from().equals(curent.getId())&&x.getTo().contains(selected.getId()))||(x.getId_from().equals(selected.getId())&&x.getTo().contains(curent.getId()));};
            List<Message>list= StreamSupport.stream(msgs.spliterator(),false)
                    .filter(filtru1)
                    .collect(Collectors.toList());
            list.sort(Comparator.comparing(Message::getId));//se vor afisa cronologic
            list_messages.getItems().setAll(list);


            txt_mesaj.clear();
            AlertaBox.display("succes","reply trimis");
        }else{
            txt_mesaj.clear();
            AlertaBox.display("error","trebuie selectat un mesaj primit");
        }

    }

    /**
     * fct prin care se afiseaza in tabel userii ce contin in email,nume sau prenume stringul cautat
     */
    public void handleSearch(){
        if(txt_search.getText().isEmpty()){

            AlertaBox.display("atentie","va rugam introduceti text");
            return;
        }
        String email=txt_search.getText();
        List<Utilizator>cautati=StreamSupport.stream(ui.utilizatorService.getSearch(email).spliterator(),false)
                .collect(Collectors.toList());
        txt_search.clear();
        //paginator.setPageFactory(this::createPage);

        useri.setAll(cautati);
        paginator.setPageFactory(this::createPage);

        tableColumnfirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableColumnlastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        tableUsers.setItems(useri);
        if(cautati.size()%5==0)
            paginator.setPageCount(cautati.size()/5);
        else
            paginator.setPageCount(cautati.size()/5+1);
        paginator.setPageFactory(this::createPage);
    }

    /**
     * la selectarea unui user din tabel se va incarca tabelul cu conversatia dintre mine si el
     */
    public void handleSelection(){
        Utilizator selected=tableUsers.getSelectionModel().getSelectedItem();
        Iterable<Message>msgs=ui.messageService.getAll();
        Predicate<Message> filtru1= x->{return (x.getId_from().equals(curent.getId())&&x.getTo().contains(selected.getId()))||(x.getId_from().equals(selected.getId())&&x.getTo().contains(curent.getId()));};
        List<Message>list= StreamSupport.stream(msgs.spliterator(),false)
                .filter(filtru1)
                .collect(Collectors.toList());
        list.sort(Comparator.comparing(Message::getId));//se vor afisa cronologic
        list_messages.getItems().setAll(list);
    }
    public void init(){
        //tabelul cu toti userii
       /* List<Utilizator> useri2=StreamSupport.stream(ui.utilizatorService.getAll().spliterator(),false)
                .collect(Collectors.toList());
        useri.setAll(useri2);*/
        //formez lista de pus in useri
        List<Utilizator>useri2=ui.utilizatorService.getUsersOnPage(0);
        //in loc de 0 sa pun paginator.getcurrentpage index
        useri.setAll(useri2);
        tableColumnfirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableColumnlastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        tableUsers.setItems(useri);
        paginator.setMaxPageIndicatorCount(1);

        if(ui.utilizatorService.getSize()<=5)
            paginator.setPageCount(1);
        else{
            if(ui.utilizatorService.getSize()%5==0)
                paginator.setPageCount(ui.utilizatorService.getSize()/5);
            else
                paginator.setPageCount(ui.utilizatorService.getSize()/5+1);
        }
        //paginator.setPageFactory(this::createPage);
        paginator.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer param) {
               List<Utilizator>l= ui.utilizatorService.getUsersOnPage(paginator.getCurrentPageIndex());
                useri.setAll(l);
                tableUsers.setItems(useri);
                return tableUsers;
            }
        });
    }
    public Node createPage(int pageIndex){
        int fromIndex=pageIndex*5;
        int toIndex=Math.min(fromIndex+5,useri.size());
        tableUsers.setItems(FXCollections.observableArrayList(useri.subList(fromIndex,toIndex)));
        return tableUsers;
    }
}
