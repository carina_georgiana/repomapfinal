package socialnetwork.Controllers;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableStringValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.ui.UI;


import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
//nu il mai folosesc
public class MessagesCtrl {
    public UI ui;
    public Utilizator selectedUser;
    ObservableList<Message> primite= FXCollections.observableArrayList();
    ObservableList<Message>trimise=FXCollections.observableArrayList();
    @FXML
    TextField txt_ids;
    @FXML
    TextField txt_mesaj;
    @FXML
    Button btn_exit;
    @FXML
    Button btn_send;
    @FXML
    Button btn_reply;
    @FXML
    Label l_sent;
    @FXML
    Label l_received;
    @FXML
    Label l_mesaje;
    @FXML
    Label l_ids;
    @FXML
    TableView<Message> tableReceive;
    @FXML
    TableView<Message> tableSend;
    @FXML
    TableColumn<Message,String> tableColumnFrom;
    @FXML
    TableColumn<Message,String> tableColumnMessage;
    @FXML
    TableColumn<Message,String> tableColumnReply;
    @FXML
    TableColumn<Message,String> tableColumnTo;
    @FXML
    TableColumn<Message,String> tableColumnMesaj;
    @FXML
    TableColumn<Message,String> tableColumnReplies;


    public void initTablePrimite(){
        //OBS:NU MAI ADAUG coloanele caci le am adaugat din scene builder
        // tableView.getColumns().addAll(tableColumnId,tableColumnFirstName,tableColumnLastName,tableColumnEmail);
        tableColumnFrom.setCellValueFactory(new PropertyValueFactory<Message,String>("id_from"));
        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<Message,String>("mesaj"));
        //tableColumnReply.setCellValueFactory(new PropertyValueFactory<Message,String>("reply"));
        tableColumnReply.setCellValueFactory(param -> new ReadOnlyObjectWrapper<String>(param.getValue().toStringReply()));
        //numele campului din mesaj are aceeasi denumire ca aici
        tableReceive.setItems(primite);
    }
    public void init(){
        initPrimite();
        initTablePrimite();
        initTrimise();
        initTableTrimise();
    }
    public void initPrimite(){
        Iterable<Message>msg_primite=ui.messageService.getAll();
        List<Message> list= StreamSupport.stream(msg_primite.spliterator(),false)
                .filter(x->x.getTo().contains(selectedUser.getId()))
                .collect(Collectors.toList());
        primite.setAll(list);
    }
    public void initTrimise(){
        Iterable<Message>msg_trimise=ui.messageService.getAll();
        List<Message>lista=StreamSupport.stream(msg_trimise.spliterator(),false)
                .filter(x->x.getId_from().equals(selectedUser.getId()))
                .collect(Collectors.toList());
        trimise.setAll(lista);
    }
    public void initTableTrimise(){
        tableColumnTo.setCellValueFactory(new PropertyValueFactory<Message,String>("to"));
        tableColumnMesaj.setCellValueFactory(new PropertyValueFactory<Message,String>("mesaj"));
        //tableColumnReplies.setCellValueFactory(new PropertyValueFactory<Message,String>("reply"));
        //param=mesaj
        tableColumnReplies.setCellValueFactory(param -> new ReadOnlyObjectWrapper<String>(param.getValue().toStringReply()));
        //numele campului din mesaj are aceeasi denumire ca aici
        tableSend.setItems(trimise);
    }
   /* public void handleExit(ActionEvent event){
        try{
        FXMLLoader loginParent=new FXMLLoader();
        loginParent.setLocation(getClass().getResource("/LoginFxml.fxml"));
        AnchorPane loginLayout=(AnchorPane)loginParent.load();
        //Scene loginScene=new Scene((Parent)loginParent);
        //iau stage din main!!!!
        Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(new Scene(loginLayout));
        LoginCtrl loginCtrl=loginParent.getController();//seteaza controller pt fxml
        loginCtrl.ui=ui;//daca mai am eu comp java de setat pt controller
        window.show();}catch(Exception e){
            e.printStackTrace();
        }
    }*/
    //public void handleExit() {
    //    System.exit(0);
    //}
    public void handleSend()throws ValidationException{
        if(txt_mesaj.getText().isEmpty()||txt_ids.getText().isEmpty()){
            AlertaBox.display("error","mesaj sau id null");
            return;
        }
        //nu validez id uri existente
        if(txt_ids.getText().matches("[0-9,]+")){
            List<String>id_strings= Arrays.asList(txt_ids.getText().split(","));
            List<Long>ids_to=new ArrayList<>();
            for(String i:id_strings)
                ids_to.add(Long.parseLong(i));
            String mesaj=txt_mesaj.getText();
            Long id_from=selectedUser.getId();
           // Utilizator fromUser=selectedUser;
            //List<Utilizator>toUseri=new ArrayList<>();
            /*for(Long id:ids_to){
                try{
                    Utilizator u=ui.utilizatorService.findId(id);
                    toUseri.add(u);
                }catch(Exception e){
                    txt_ids.clear();
                    txt_mesaj.clear();
                    AlertaBox.display("error","nu exista user cu acel id");
                    return;
                }
            }*/
            Message nou=new Message(id_from,ids_to,mesaj);
            //setez id mesaj-in baza de date se face asta,cu autoincrement
           /* Iterable<Message>mesaje=ui.messageService.getAll();
            List<Message>lista_mesaje= new ArrayList<>();
            for(Message fr:mesaje)
            {
                lista_mesaje.add(fr);
            }
            for(Message me:lista_mesaje)
                if(me.getId()>ui.id_max)
                    ui.id_max=me.getId();
            nou.setId(ui.id_max+1);*/
            //Message rez=messageService.save(m);
            ui.messageService.save(nou);
            init();
            txt_mesaj.clear();
            txt_ids.clear();
            AlertaBox.display("succes","mesaj trimis");
        }else{
            txt_ids.clear();
            AlertaBox.display("error","idurile sunt numere");
            return;
        }
    }
    public void handleReply()throws ValidationException {
        Message selectedMsg=tableReceive.getSelectionModel().getSelectedItem();
        if(selectedMsg!=null){
                if(txt_mesaj.getText().isEmpty()){
                    txt_mesaj.clear();
                    AlertaBox.display("error","mesajul nu are text");
                    return;
                }
                String mesaj=txt_mesaj.getText();
                Long id_to=selectedMsg.getId_from();
                List<Long>ids_to=new ArrayList<Long>();
                ids_to.add(id_to);
                Long id_from=selectedUser.getId();
                //formez msg de reply=cel selectat?
                Message nou=new Message(id_from,ids_to,mesaj,selectedMsg);
                ui.messageService.save(nou);
               // ui.messageService.delete(selectedMsg.getId());
                //selectedMsg.setReply(nou);
                //ui.messageService.save(selectedMsg);
                //init tabele
                init();
                txt_mesaj.clear();
                AlertaBox.display("succes","reply trimis");
        }else{
            txt_mesaj.clear();
            AlertaBox.display("error","trebuie selectat un mesaj primit");
        }
    }

    public void handleExit(javafx.event.ActionEvent event) {
        try{
            FXMLLoader loginParent=new FXMLLoader();
            loginParent.setLocation(getClass().getResource("/LoginFxml.fxml"));
            AnchorPane loginLayout=(AnchorPane)loginParent.load();
            //Scene loginScene=new Scene((Parent)loginParent);
            //iau stage din main!!!!
            Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(new Scene(loginLayout));
            LoginCtrl loginCtrl=loginParent.getController();//seteaza controller pt fxml
            loginCtrl.ui=ui;//daca mai am eu comp java de setat pt controller
            window.show();}catch(Exception e){
            e.printStackTrace();
        }
    }
}
