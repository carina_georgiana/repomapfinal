package socialnetwork.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.effect.PerspectiveTransform;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import socialnetwork.BCrypt.MyCrypt;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.EventService;
import socialnetwork.ui.UI;

import java.io.IOException;

public class LoginCtrl {
    //mai am de deschis fereastra pt cont nou-sign in;apoi trec la user curent
    public  UI ui;
    public Utilizator curent=null;
    public EventService eventService;
    @FXML
    Label l_logare;
    @FXML
    Label l_socialtime;
    @FXML
    Label l_email;
    @FXML
    Label l_id;
    @FXML
    Label l_signin;
    @FXML
    Button btn_signin;
    @FXML
    Button btn_logare;
    @FXML
    Button btn_exit;
    @FXML
    Label l_error;
    @FXML
    TextField txt_email;
    @FXML
    PasswordField txt_id;

    /**
     *
     * @param event-apasare buton
     * @throws IOException-daca dau calea gresita la fisierul fxml
     * fct ce deschide pagina pentru creare cont nou
     */
    public void handleSignin(ActionEvent event)throws IOException {
        FXMLLoader loginParent = new FXMLLoader();
        loginParent.setLocation(getClass().getResource("/SignIn.fxml"));
        AnchorPane loginLayout = (AnchorPane) loginParent.load();
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(loginLayout));
        SignInCtrl ctrl = loginParent.getController();//seteaza controller pt fxml
        ctrl.ui = ui;
        ctrl.eventService=eventService;
        window.show();

    }
        public void handleExit(){
        Boolean answer=ConfirmBox.display("inchidere","esti sigur?");
        if(answer==true){
            System.out.println("da");
            System.exit(0);
        }
    }
    public void handleLogare(ActionEvent event)throws IOException {
        l_error.setVisible(false);
        if(txt_email.getText().isEmpty()||txt_id.getText().isEmpty()){
            AlertaBox.display("eroare","va rugam introduceti email si parola");
            return;
        }

        Utilizator u=ui.utilizatorService.findByEmail(txt_email.getText());
        if(u==null){
            txt_id.clear();
            txt_email.clear();
            AlertaBox.display("error","nu exista utilizatorul");
            return;
        }
       // if(MyCrypt.verifyHash(u.getPassword(),MyCrypt.hash(txt_id.getText()))){
        if(MyCrypt.verifyHash(txt_id.getText(),u.getPassword()))
        {
            txt_id.clear();
            txt_email.clear();
            curent=u;
        }
        else
        {
            txt_email.clear();
            txt_id.clear();
            l_error.setVisible(true);
            return;

        }

                FXMLLoader loginParent=new FXMLLoader();
                loginParent.setLocation(getClass().getResource("/UserCurent.fxml"));
                AnchorPane loginLayout=(AnchorPane)loginParent.load();
                Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
                window.setScene(new Scene(loginLayout));
                UserCurentCtrl loginCtrl=loginParent.getController();
                loginCtrl.ui=ui;
                loginCtrl.eventService=eventService;
                loginCtrl.selectedUser=curent;
                loginCtrl.init();
                window.show();

    }

    /**
     * fct ce pune efecte pe titlu
     */
    public void init(){
        DropShadow shadow=new DropShadow();
        shadow.setOffsetX(8);
        shadow.setOffsetY(8);
        shadow.setColor(Color.BLACK);
        l_socialtime.setEffect(shadow);


        //l_socialtime.getContentDisplay().add(r);
        //l_socialtime.getChildrenUnmodifiable().add(t);

        //Reflection refl=new Reflection();
        //refl.setFraction(10);
        //refl.setTopOffset(4);
        //l_socialtime.setEffect(refl);

    }
}
