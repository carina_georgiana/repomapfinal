package socialnetwork.Controllers;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.EventService;
import socialnetwork.ui.UI;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
//nu am dat conexiuni si denumiri in fisierul fxml
public class UserCurentCtrl implements Observer
//implements Observer{
{
    public UI ui;
    public EventService eventService;
    public Utilizator selectedUser;
    @FXML
    CheckBox check_friends;
    @FXML
    Button btn_search;
    @FXML
    Button btn_events;
    @FXML
    Button btn_notificari;
    @FXML
    Label l_welcome;
    @FXML
    Button btn_delete;
    @FXML
    Button btn_add;
    @FXML
    Button btn_requests;
    @FXML
    Button btn_logout;
    @FXML
    Button btn_accept;
    @FXML
    Button btn_cancel;
    @FXML
    Button btn_messages;
    @FXML
    Button btn_rapoarte;
    @FXML
    TableView<Utilizator> tableView;
    @FXML
    TableColumn<Utilizator,String> tableColumnFirstName;
    @FXML
    TableColumn<Utilizator,String> tableColumnLastName;
    @FXML
    TableColumn<Utilizator,String> tableColumnId;
    @FXML
    TableColumn<Utilizator,String> tableColumnEmail;

    ObservableList<Utilizator>friends=FXCollections.observableArrayList();
    ObservableList<Prietenie>friendships=FXCollections.observableArrayList();//date,e1,e2
    @FXML
    TableView<Prietenie> tableRequests;
    @FXML
    TableColumn<Prietenie,String> tableColumnIdFriend;
    @FXML
    TableColumn<Prietenie,String> tableColumnDataFriend;
    @FXML
    TableColumn<Prietenie,String>tableColumnStatus;
    @FXML
    TextField txt_search;
    @FXML
    MenuButton btn_menu;
    @FXML
    Label l_table2;
    @FXML
    Label l_notificari;
    @FXML
    Pagination pagination;
    @FXML
    Pagination paginator2;
    int flag=1;

    /**
     * fct ce incarca in label nr de notificari dorite
     */
    public void nr_notificari(){
        int nr_notificari=eventService.nr_notificari_viitoare(selectedUser.getId());
        l_notificari.setText(String.valueOf(nr_notificari));
    }
    public void handleEvents(ActionEvent event)throws IOException{
            FXMLLoader rapParent=new FXMLLoader();
            rapParent.setLocation(getClass().getResource("/EventsFxml.fxml"));
            AnchorPane layout=(AnchorPane)rapParent.load();
            Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(new Scene(layout));
            EventsCtrl ctrl=rapParent.getController();
            ctrl.ui=ui;
            ctrl.curent=selectedUser;
            ctrl.eventService=eventService;
            ctrl.flag=flag;
            ctrl.init();
            window.show();
        }

    public void handleNotificari(ActionEvent event)throws IOException{
        FXMLLoader rapParent=new FXMLLoader();
        rapParent.setLocation(getClass().getResource("/Notificari.fxml"));
        AnchorPane layout=(AnchorPane)rapParent.load();
        Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(new Scene(layout));
        NotificariCtrl ctrl=rapParent.getController();
        ctrl.ui=ui;
        ctrl.curent=selectedUser;
        ctrl.eventService=eventService;
        ctrl.init();
        flag=0;
        window.show();
    }
    public void handleSearch(){
        if(txt_search.getText().isEmpty()){

            AlertaBox.display("atentie","va rugam introduceti text");
            return;
        }
        String email=txt_search.getText();
        List<Utilizator>cautati=StreamSupport.stream(ui.utilizatorService.getSearch(email).spliterator(),false)
                .collect(Collectors.toList());
        txt_search.clear();
        friends.setAll(cautati);
        initTable();
        if(cautati.size()%5==0)
            pagination.setPageCount(cautati.size()/5);
        else
            pagination.setPageCount(cautati.size()/5+1);
        pagination.setPageFactory(this::createPage);
    }
    public void handleCheck(){
        //checkBox bifat=>afisez toti userii
        if(check_friends.isSelected())
        {
            //incarc
            initTable();
            initFriends();
            pagination.setPageFactory(this::createPage);
            check_friends.setText("Friends");
        }
        else{
            //incarc tabel cu toti userii
            initTable();
            Iterable<Utilizator>userii= ui.utilizatorService.getAll();
            List<Utilizator>useri=StreamSupport.stream(userii.spliterator(),false)
                    .collect(Collectors.toList());
            friends.setAll(useri);
            if(useri.size()%5==0)
                pagination.setPageCount(useri.size()/5);
            else
                pagination.setPageCount(useri.size()/5+1);
            pagination.setPageFactory(this::createPage);
            check_friends.setText("All users");
        }
    }
    public void handleLogout(ActionEvent event){
        try{
            FXMLLoader loginParent=new FXMLLoader();
            loginParent.setLocation(getClass().getResource("/LoginFxml.fxml"));
            AnchorPane loginLayout=(AnchorPane)loginParent.load();
            Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(new Scene(loginLayout));
            LoginCtrl loginCtrl=loginParent.getController();
            loginCtrl.ui=ui;
            loginCtrl.eventService=eventService;
            loginCtrl.init();
            window.show();}catch (IOException e){
            e.printStackTrace();
        }
    }
    public void handleDelete()throws ValidationException {
        Utilizator selectedFriend=tableView.getSelectionModel().getSelectedItem();
        if(selectedFriend!=null){
            Prietenie deletedFriend=ui.prietenieService.removePrietenie(selectedUser.getId(),selectedFriend.getId());
            selectedFriend.removePrieten(selectedUser);
            selectedUser.removePrieten(selectedFriend);
            tableRequests.setVisible(false);
            tableRequests.setEditable(false);
            tableRequests.setDisable(true);
            l_table2.setVisible(false);
            l_table2.setDisable(true);
            paginator2.setDisable(true);
            paginator2.setVisible(false);
            init();
            AlertaBox.display("succes","prieten sters");
            }else{
            AlertaBox.display("error","trebuie selectat un prieten");
        }
    }
    public void initTable(){
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        tableColumnId.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("id"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        //numele campului din user are aceeasi denumire ca aici
        tableView.setItems(friends);
    }
    public Node createPage(int pageIndex){
        int fromIndex=pageIndex*5;
        int toIndex=Math.min(fromIndex+5,friends.size());
        tableView.setItems(FXCollections.observableArrayList(friends.subList(fromIndex,toIndex)));
        return tableView;

    }
    public void init(){
        ui.prietenieService.addObserver(this);
        initTable();
        pagination.setMaxPageIndicatorCount(1);
        initFriends();
        pagination.setPageFactory(this::createPage);
        //System.out.println(eventService);
        if(flag==1)
        { nr_notificari();}
        else{l_notificari.setText("0");}
        check_friends.setSelected(true);
        check_friends.setText("Friends");
        l_welcome.setText("Welcome to your page, "+selectedUser.getFirstName().toUpperCase()+" "+selectedUser.getLastName().toUpperCase()+"!");
        //paginator2.getCurrentPageIndex()-da 0 pt pag 1
       // paginator2.currentPageIndexProperty().addListener(x->initFriends());
        //in initfrineds aduc pagina cu index curent din getall din useri!!!!cu fct service
        //pt prieteni-aduc pagina cu prietenii mei din service de prietenii
        
    }
    public void initFriends(){
        //momentan curent nu are prieteni,aici ii setez
        List<Utilizator>prieteni=StreamSupport.stream(ui.utilizatorService.init_prieteni(selectedUser.getId()).spliterator(),false)
                .collect(Collectors.toList());
        for(Utilizator ut:prieteni)
            selectedUser.addPrieten(ut);//acum am prieteni
        pagination.setPageFactory(this::createPage);
        if(prieteni.size()<=5)
        {pagination.setPageCount(1);}
        else{
            if(prieteni.size()%5==0)
                pagination.setPageCount(prieteni.size()/5);
            else
                pagination.setPageCount(prieteni.size()/5+1);}
        friends.setAll(prieteni);
    }
    public void handleAdd(){
        Utilizator selectedFriend=tableView.getSelectionModel().getSelectedItem();
        if(selectedFriend==null){
            AlertaBox.display("atentie","te rog alege un user");
            return;
        }
        Long id2=selectedFriend.getId();
        Tuple<Long,Long> tuple=new Tuple<>(selectedUser.getId(), id2);
        Tuple<Long,Long>tuple2=new Tuple<>(id2,selectedUser.getId());
        //obs:daca prietenia a fost refuzata o data,nu mai pot trimite->previn raportul
        if(ui.prietenieService.findId(tuple)!=null||ui.prietenieService.findId(tuple2)!=null||selectedUser.getId().equals(id2)){
            AlertaBox.display("atentie","sunteti deja prieteni");
            return;
        }
        Prietenie p=new Prietenie(selectedUser.getId(),id2, LocalDateTime.now());
        ui.prietenieService.save(p);
        //nu se schimba
        tableRequests.setVisible(false);
        tableRequests.setEditable(false);
        tableRequests.setDisable(true);
        l_table2.setDisable(true);
        l_table2.setVisible(false);
        paginator2.setDisable(true);
        paginator2.setVisible(false);

        AlertaBox.display("succes","cerere trimisa cu succes");
    }
    public void handleRequests(){
        tableRequests.setVisible(true);
        tableRequests.setEditable(true);
        tableRequests.setDisable(false);
        l_table2.setDisable(false);
        l_table2.setVisible(true);
        paginator2.setDisable(false);
        paginator2.setVisible(true);
        List<Prietenie>list=StreamSupport.stream(ui.prietenieService.my_friendships(selectedUser.getId()).spliterator(),false)
                .collect(Collectors.toList());
        friendships.setAll(list);
        tableColumnIdFriend.setCellValueFactory(param->new ReadOnlyObjectWrapper<String>(ui.utilizatorService.findId(param.getValue().getId().getLeft()).getEmail()+"\n"+ui.utilizatorService.findId(param.getValue().getId().getRight()).getEmail()));
        tableColumnDataFriend.setCellValueFactory(new PropertyValueFactory<Prietenie,String>("date"));
        tableColumnStatus.setCellValueFactory(new PropertyValueFactory<Prietenie,String>("status"));
        tableRequests.setItems(friendships);
        paginator2.setMaxPageIndicatorCount(1);

        if(friendships.size()<=5)
            paginator2.setPageCount(1);
        else{
            if(ui.utilizatorService.getSize()%5==0)
                paginator2.setPageCount(friendships.size()/5);
            else
                paginator2.setPageCount(friendships.size()/5+1);
        }
        paginator2.setPageFactory(this::createPage2);
    }
    public Node createPage2(int pageIndex){
        int fromIndex=pageIndex*5;
        int toIndex=Math.min(fromIndex+5,friendships.size());
        tableRequests.setItems(FXCollections.observableArrayList(friendships.subList(fromIndex,toIndex)));
        return tableRequests;
    }
    public void handleAccept(){
        Prietenie selectedFriendship=tableRequests.getSelectionModel().getSelectedItem();
        if(selectedFriendship!=null){
            if(selectedFriendship.getId().getLeft().equals(selectedUser.getId())){
                AlertaBox.display("error","nu iti poti raspunde la propria cerere");
                return;
            }
            if(!selectedFriendship.getStatus().equals("pending")){
                AlertaBox.display("error","ati raspuns deja la cerere");
                return;
            }
            boolean answer=ConfirmBox.display("cerere","accepti cererea?");
            if(answer==false){
                ui.prietenieService.delete(selectedFriendship.getId());
                selectedFriendship.setStatus("rejected");
                ui.prietenieService.save(selectedFriendship);
                tableRequests.setDisable(true);
                tableRequests.setVisible(false);
                tableRequests.setEditable(false);
                l_table2.setDisable(true);
                l_table2.setVisible(false);
                paginator2.setDisable(true);
                paginator2.setVisible(false);

                AlertaBox.display("succes","cererea a fost refuzata");
            }
            if(answer==true){
                ui.prietenieService.delete(selectedFriendship.getId());
                selectedFriendship.setStatus("accepted");
                ui.prietenieService.save(selectedFriendship);
                //obs:data prieteniei nu se schimba,daca vreau sa o schimb:
                //selectedFriendship.setDate(Localdatetime.now),apoi ui.save

                Utilizator friend=ui.utilizatorService.findId(selectedFriendship.getId().getLeft());
                selectedUser.addPrieten(friend);
                friend.addPrieten(selectedUser);
                initFriends();
                tableView.setItems(friends);
                tableRequests.setDisable(true);
                tableRequests.setVisible(false);
                tableRequests.setEditable(false);
                l_table2.setDisable(true);
                l_table2.setVisible(false);
                paginator2.setDisable(true);
                paginator2.setVisible(false);

                AlertaBox.display("succes","cerere acceptata");
            }

        }else{
            AlertaBox.display("error","trebuie selectata o cerere");
        }
    }
    public void handleCancel(){
        Prietenie selectedFriendship=tableRequests.getSelectionModel().getSelectedItem();
        if(selectedFriendship!=null){
            if(selectedFriendship.getId().getRight().equals(selectedUser.getId())){
                AlertaBox.display("error","nu poti retrage decat propriile cereri");
                return;
            }
            if(!selectedFriendship.getStatus().equals("pending")){
                AlertaBox.display("error","s-a raspuns deja la cerere");
                return;
            }
            boolean answer=ConfirmBox.display("cerere","retragi cererea?");
            //daca raspund cu false=>nu fac nimic
            if(answer==true){
                ui.prietenieService.delete(selectedFriendship.getId());
                //reload tableview+alertbox
                Iterable<Prietenie>prietenii=ui.prietenieService.getAll();

                List<Prietenie>list=StreamSupport.stream(ui.prietenieService.my_friendships(selectedUser.getId()).spliterator(),false)
                        .collect(Collectors.toList());
                friendships.setAll(list);
                tableColumnIdFriend.setCellValueFactory(param->new ReadOnlyObjectWrapper<String>(ui.utilizatorService.findId(param.getValue().getId().getLeft()).getEmail()+"\n"+ui.utilizatorService.findId(param.getValue().getId().getRight()).getEmail()));

                tableColumnDataFriend.setCellValueFactory(new PropertyValueFactory<Prietenie,String>("date"));
                tableColumnStatus.setCellValueFactory(new PropertyValueFactory<Prietenie,String>("status"));
                tableRequests.setItems(friendships);

                paginator2.setMaxPageIndicatorCount(1);

                if(friendships.size()<=5)
                    paginator2.setPageCount(1);
                else{
                    if(ui.utilizatorService.getSize()%5==0)
                        paginator2.setPageCount(friendships.size()/5);
                    else
                        paginator2.setPageCount(friendships.size()/5+1);
                }
                paginator2.setPageFactory(this::createPage2);

                AlertaBox.display("succes","cerere retrasa");
            }

        }else{
            AlertaBox.display("error","trebuie selectata o cerere");
        }
    }
    public void handleMessages(ActionEvent event)throws IOException{
        FXMLLoader loginParent=new FXMLLoader();
        loginParent.setLocation(getClass().getResource("/Messages2.fxml"));
        AnchorPane loginLayout=(AnchorPane)loginParent.load();
        Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(new Scene(loginLayout));

        MessagesCtrl2 ctrl=loginParent.getController();
        ctrl.ui=ui;
        ctrl.curent=selectedUser;
        ctrl.flag=flag;
        ctrl.eventService=eventService;
        ctrl.init();
        window.show();
    }

    @Override
    public void update() {
        initFriends();
        tableView.setItems(friends);
    }
    public void handleRapoarte(ActionEvent event)throws IOException{
        FXMLLoader rapParent=new FXMLLoader();
        rapParent.setLocation(getClass().getResource("/RapoarteFxml.fxml"));
        AnchorPane layout=(AnchorPane)rapParent.load();
        Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(new Scene(layout));
        RapoarteCtrl ctrl=rapParent.getController();
        ctrl.ui=ui;
        ctrl.curent=selectedUser;
        ctrl.eventService=eventService;
        ctrl.flag=flag;
        ctrl.init();
        window.show();
    }
}
