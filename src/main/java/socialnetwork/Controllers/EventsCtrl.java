package socialnetwork.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.service.EventService;
import socialnetwork.ui.UI;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class EventsCtrl {
    UI ui;
    EventService eventService;
    Utilizator curent;
    @FXML
    ListView<Event> list_events;
    @FXML
    Pagination pagination;
    @FXML
    Button btn_stop;
    @FXML
    Button btn_start;
    @FXML
    Label l_events;
    @FXML
    Label l_titlu;
    @FXML
    Label l_tabel;
    @FXML
    Label l_descriere;
    @FXML
    Label l_data;
    @FXML
    TextField txt_titlu;
    @FXML
    TextField txt_descriere;
    @FXML
    DatePicker txt_data;
    @FXML
    Button btn_back;
    @FXML
    Button btn_abonare;
    @FXML
    Button btn_dezabonare;
    @FXML
    Button btn_new;
    @FXML
    CheckBox btn_check;
    int flag=0;
    ObservableList<Event> events= FXCollections.observableArrayList();

    /**
     * fct ce ia eventul selectat si opreste notificarile pt el.
     */
    public void handleStop(){
        Event selectedEvent=list_events.getSelectionModel().getSelectedItem();
        if(selectedEvent!=null){
            if(eventService.findOneMN(curent.getId(),selectedEvent.getId())==null){
                AlertaBox.display("error","nu sunteti abonat la event");
                return;
            }
            if(eventService.findOneMN(curent.getId(),selectedEvent.getId()).equals(new Tuple<Long,Long>(0L,0L))){
                eventService.stop_notify(curent.getId(),selectedEvent.getId());
                //daca eventul nu e pe mute deja
                AlertaBox.display("succes","evenimentul a fost dat pe mute");
                flag=1;
                return;
            }
            AlertaBox.display("atentie","evenimentul este deja dat pe mute");
        }else{
            AlertaBox.display("error","selecteaza un event");
        }
    }

    /**
     * fct ce ia evenimentul selectat si porneste notificarile pt el
     */
    public void handleStart(){
        Event selectedEvent=list_events.getSelectionModel().getSelectedItem();
        if(selectedEvent!=null){
            if(eventService.findOneMN(curent.getId(),selectedEvent.getId())==null){
                AlertaBox.display("error","nu sunteti abonat la event");
                return;
            }
            if(eventService.findOneMN(curent.getId(),selectedEvent.getId()).equals(new Tuple<Long,Long>(0L,0L))){
                AlertaBox.display("atentie","evenimentul nu este pe mute");
                return;
            }
            eventService.start_notify(curent.getId(),selectedEvent.getId());
            AlertaBox.display("succes","evenimentul este acum pe unmute");
            flag=1;
        }else{
            AlertaBox.display("error","selecteaza un event");
        }
    }

    /**
     *
     * @param event-actiunea de apasare buton back
     *             ma intoarce la pagina principala a userului curent
     */
    public void handleBack(ActionEvent event){
        try{
            FXMLLoader loginParent=new FXMLLoader();
            loginParent.setLocation(getClass().getResource("/UserCurent.fxml"));
            AnchorPane loginLayout=(AnchorPane)loginParent.load();
            //Scene loginScene=new Scene((Parent)loginParent);
            //iau stage din main!!!!
            Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(new Scene(loginLayout));
            UserCurentCtrl loginCtrl=loginParent.getController();//seteaza controller pt fxml
            loginCtrl.ui=ui;//daca mai am eu comp java de setat pt controller
            loginCtrl.selectedUser=curent;
            loginCtrl.eventService=eventService;
            loginCtrl.flag=flag;
            loginCtrl.init();
            window.show();}catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * fct prin care ma abonez la evenimentul selectat din lista
     */
    public void handleAbonare(){
        Event selected=list_events.getSelectionModel().getSelectedItem();
        //imi trebuie findonemn din tabelul de rel m-n
        if(eventService.findOneMN(curent.getId(),selected.getId())==null){
            //ma pot abona
            if(selected.getDate().isBefore(LocalDate.now())){
                AlertaBox.display("atentie","evenimentul a trecut deja!");
                return;
            }
            eventService.abonare(curent.getId(),selected.getId());
            if(btn_check.isSelected()){
                //reload lista
                Iterable<Event>events2=eventService.abonat_la(curent.getId());
                List<Event>list= StreamSupport.stream(events2.spliterator(),false)
                        .filter(x->x.getDate().isAfter(LocalDate.now()))
                        .collect(Collectors.toList());
                events.setAll(list);
                list_events.getItems().setAll(events);

                if(events.size()<=5)
                    pagination.setPageCount(1);
                else{
                    if(events.size()%5==0)
                        pagination.setPageCount(events.size()/5);
                    else
                        pagination.setPageCount(events.size()/5+1);
                }
                pagination.setPageFactory(this::createPage);
            }
            flag=1;
            AlertaBox.display("succes","te ai abonat la event");
        }
        else
        {
            AlertaBox.display("atentie","esti deja abonat");
            return;
        }
        flag=1;
    }

    /**
     * fct prin care ma dezabonez de la un eveniment selectat
     */
    public void handleDezabonare(){
        Event selected=list_events.getSelectionModel().getSelectedItem();
        //imi trebuie findonemn din tabelul de rel m-n
        if(eventService.findOneMN(curent.getId(),selected.getId())!=null){
            //ma pot dezabona
            if(selected.getDate().isBefore(LocalDate.now())){
                AlertaBox.display("atentie","evenimentul a trecut deja!");
                return;
            }
            eventService.dezabonare(curent.getId(),selected.getId());
            if(btn_check.isSelected()){
                //reload lista
                Iterable<Event>events2=eventService.abonat_la(curent.getId());
                List<Event>list= StreamSupport.stream(events2.spliterator(),false)
                        .filter(x->x.getDate().isAfter(LocalDate.now()))
                        .collect(Collectors.toList());
                events.setAll(list);
                list_events.getItems().setAll(events);

                if(events.size()<=5)
                    pagination.setPageCount(1);
                else{
                    if(events.size()%5==0)
                        pagination.setPageCount(events.size()/5);
                    else
                        pagination.setPageCount(events.size()/5+1);
                }
                pagination.setPageFactory(this::createPage);
            }
            flag=1;
            AlertaBox.display("succes","te ai dezabonat de la event");
        }
        else
        {
            AlertaBox.display("atentie","nu esti abonat");
            return;
        }
        flag=1;
    }

    /**
     * creare event nou
     */
    public void handleNew(){
        //creez event nou,daca am lista din tabel pe all events(check nebifat)=>reload listview;
        if(txt_titlu.getText().isEmpty()||txt_descriere.getText().isEmpty()){
            AlertaBox.display("error","va rugam completati toate campurile");
            return;
        }
        if(txt_data.getValue().isBefore(LocalDate.now())){
            AlertaBox.display("atentie","evenimentul nu poate fi in trecut");
            return;
        }
        Event e=new Event(curent.getId(),txt_titlu.getText(),txt_descriere.getText(),txt_data.getValue());
        eventService.save(e);
        //acum i s a atribuit id automat
        if(!btn_check.isSelected()){
            //reload lista
            Iterable<Event>events2=eventService.getAll();
            List<Event>list= StreamSupport.stream(events2.spliterator(),false)
                    .filter(x->x.getDate().isAfter(LocalDate.now()))
                    .collect(Collectors.toList());
            events.setAll(list);
            list_events.getItems().setAll(events);

            if(events.size()<=5)
                pagination.setPageCount(1);
            else{
                if(events.size()%5==0)
                    pagination.setPageCount(events.size()/5);
                else
                    pagination.setPageCount(events.size()/5+1);
            }
            pagination.setPageFactory(this::createPage);
        }
        AlertaBox.display("succes","eveniment nou creat!");
        txt_titlu.clear();
        txt_descriere.clear();
    }

    /**
     * daca e bifat checkBox ul,afisez evenimentele viitoare la care m am abonat
     * altfel=>afisez toate evenimentele viitoare
     */
    public void handleCheck(){
        if(btn_check.isSelected())
        {
            //pagination.setPageFactory(this::createPage);
            initList();

            /*Iterable<Event>events2=eventService.abonat_la(curent.getId());
            List<Event>list= StreamSupport.stream(events2.spliterator(),false)
                    .filter(x->x.getDate().isAfter(LocalDate.now()))
                    .collect(Collectors.toList());
            events.setAll(list);
            list_events.getItems().setAll(events);*/
            l_tabel.setText("MyEvents");
        }
        else{
            //incarc tabel cu toate events viitoare
            /*
            Iterable<Event>events2=eventService.getAll();
            List<Event>list= StreamSupport.stream(events2.spliterator(),false)
                    .filter(x->x.getDate().isAfter(LocalDate.now()))
                    .collect(Collectors.toList());
            events.setAll(list);
            list_events.getItems().setAll(events);*/
            list_events.getItems().setAll(events);
            Iterable<Event>eventss= eventService.getAll();
            List<Event>event=StreamSupport.stream(eventss.spliterator(),false)
                    .filter(x->x.getDate().isAfter(LocalDate.now()))
                    .collect(Collectors.toList());
            events.setAll(event);
            if(events.size()%5==0)
                pagination.setPageCount(events.size()/5);
            else
                pagination.setPageCount(events.size()/5+1);
            pagination.setPageFactory(this::createPage);

            l_tabel.setText("All events");
        }}

    /**
     * fct ce incarca lista cu evenimentele la care sunt abonat
     */
    public void init(){
           /* initList();
            btn_check.setSelected(true);*/
        pagination.setMaxPageIndicatorCount(1);
        //setez lista pe tabel
        initList();
        pagination.setPageFactory(this::createPage);
        btn_check.setSelected(true);

    }
        public void initList(){
            Iterable<Event>events2=eventService.abonat_la(curent.getId());
            List<Event>list= StreamSupport.stream(events2.spliterator(),false)
                    .filter(x->x.getDate().isAfter(LocalDate.now()))
                    .collect(Collectors.toList());

            events.setAll(list);
            list_events.getItems().setAll(events);

            pagination.setPageFactory(this::createPage);
            if(list.size()<=5)
            {pagination.setPageCount(1);}
            else{
                if(list.size()%5==0)
                    pagination.setPageCount(list.size()/5);
                else
                    pagination.setPageCount(list.size()/5+1);}

        }
    public Node createPage(int pageIndex){
        int fromIndex=pageIndex*5;
        int toIndex=Math.min(fromIndex+5,events.size());
        list_events.setItems(FXCollections.observableArrayList(events.subList(fromIndex,toIndex)));
        return list_events;

    }
}
