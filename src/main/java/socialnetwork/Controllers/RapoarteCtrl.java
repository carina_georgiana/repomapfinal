package socialnetwork.Controllers;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.EventService;
import socialnetwork.ui.UI;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class RapoarteCtrl {
    Utilizator curent;
    UI ui;
    EventService eventService;
    @FXML
    TableView<Utilizator> tableUtilizatori;
    @FXML
    Label l_mesaje;
    @FXML
    Pagination paginator;
    @FXML
    TableView<Message> tableMesaje;
    @FXML
    TextField txt_email;
    @FXML
    DatePicker dataPicker1;
    @FXML
    DatePicker dataPicker2;
    @FXML
    Label l_titlu;
    @FXML
    Label l_date1;
    @FXML
    Label l_date2;
    @FXML
    Button btn_back;
    @FXML
    Button btn_search;
    @FXML
    Button btn_conversatie;
    @FXML
    Button btn_activitate;
    @FXML
    Button btn_pdfConv;
    @FXML
    Button btn_pdfAct;
    @FXML
    TableColumn<Message,String> tableColumnFrom;
    @FXML
    TableColumn<Message,String> tableColumnMessage;
    @FXML
    TableColumn<Message,String> tableColumnReply;
    @FXML
    TableColumn<Utilizator,String> tableColumnFirstName;
    @FXML
    TableColumn<Utilizator,String> tableColumnLastName;
    @FXML
    TableColumn<Utilizator,String> tableColumnEmail;
    @FXML
    Label l_friends;
    ObservableList<Utilizator> friends= FXCollections.observableArrayList();
    ObservableList<Message>messages=FXCollections.observableArrayList();
    int flag=0;

    public Node createPage(int pageIndex){
        int fromIndex=pageIndex*5;
        int toIndex=Math.min(fromIndex+5,friends.size());
        tableUtilizatori.setItems(FXCollections.observableArrayList(friends.subList(fromIndex,toIndex)));
        return tableUtilizatori;
    }

    /**
     * fct ce incarca tabelul cu utilizatori
     */
    public void init(){
        initUseri();
        initTableUseri();
        paginator.setMaxPageIndicatorCount(1);

        if(ui.utilizatorService.getSize()<=5)
            paginator.setPageCount(1);
        else{
            if(ui.utilizatorService.getSize()%5==0)
                paginator.setPageCount(ui.utilizatorService.getSize()/5);
            else
                paginator.setPageCount(ui.utilizatorService.getSize()/5+1);
        }
        paginator.setPageFactory(this::createPage);

        tableColumnFrom.setCellValueFactory(param->new ReadOnlyObjectWrapper<String>(ui.utilizatorService.findId(param.getValue().getId_from()).getFirstName()));
        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<Message,String>("mesaj"));
        tableColumnReply.setCellValueFactory(param -> new ReadOnlyObjectWrapper<String>(param.getValue().toStringReply()));
        tableMesaje.setDisable(true);
        tableMesaje.setVisible(false);
        tableMesaje.setEditable(false);
    }
    public void initUseri(){
        List<Utilizator> lista=StreamSupport.stream(ui.utilizatorService.getAll().spliterator(),false)
                .collect(Collectors.toList());
        friends.setAll(lista);

    }
    public void initTableUseri(){
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("email"));
        //numele campului din utilizator are aceeasi denumire ca aici
        tableUtilizatori.setItems(friends);

    }
    public void handleBack(ActionEvent event){
        try{
            FXMLLoader loginParent=new FXMLLoader();
            loginParent.setLocation(getClass().getResource("/UserCurent.fxml"));
            AnchorPane loginLayout=(AnchorPane)loginParent.load();
            Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(new Scene(loginLayout));
            UserCurentCtrl loginCtrl=loginParent.getController();//seteaza controller pt fxml
            loginCtrl.ui=ui;
            loginCtrl.selectedUser=curent;
            loginCtrl.eventService=eventService;
            loginCtrl.flag=flag;
            loginCtrl.init();
            window.show();}catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * fct ce incarca in tabel lista cu userii ce contin in nume,prenume sau email stringul cautat
     */
    public void handleSearch(){
        if(txt_email.getText().isEmpty()){

            AlertaBox.display("atentie","va rugam introduceti text");
            return;
        }
        String email=txt_email.getText();
        List<Utilizator>cautati=StreamSupport.stream(ui.utilizatorService.getSearch(email).spliterator(),false)
                .collect(Collectors.toList());
        txt_email.clear();
        friends.setAll(cautati);
        initTableUseri();
        if(cautati.size()%5==0)
            paginator.setPageCount(cautati.size()/5);
        else
            paginator.setPageCount(cautati.size()/5+1);
        paginator.setPageFactory(this::createPage);
    }

    /**
     *ia user selectat,datele din pickeruri si afiseaza mesaje primite de mine de la selectat,in tabel2
     */
    public void handleConversatie(){
        LocalDate data1=dataPicker1.getValue();
        if(data1==null){
            AlertaBox.display("error","selectati o data");
            return;
        }
        LocalDate data2=dataPicker2.getValue();
        if(data2==null){
            AlertaBox.display("error","selectati data");
            return;
        }
        if(data2.isBefore(data1)||data1.equals(data2)){
            AlertaBox.display("error","data1 trebuie sa fie inainte de data2");
            return;
        }
        Utilizator selectedFriend=tableUtilizatori.getSelectionModel().getSelectedItem();
        if(selectedFriend==null){
            AlertaBox.display("error","va rugam selectati un user");
            return;
        }
        List<Message>ptmesaje=StreamSupport.stream(ui.messageService.conversatie_cu(selectedFriend.getId(),curent.getId(),data1,data2).spliterator(),false)
                .collect(Collectors.toList());
        messages.setAll(ptmesaje);
        //am setat  listele,setez tabelele
        init();//tabel 1 setat
        tableColumnFrom.setCellValueFactory(param->new ReadOnlyObjectWrapper<String>(ui.utilizatorService.findId(param.getValue().getId_from()).getFirstName()));
        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<Message,String>("mesaj"));
        tableColumnReply.setCellValueFactory(param -> new ReadOnlyObjectWrapper<String>(param.getValue().toStringReply()));
        tableMesaje.setEditable(true);
        tableMesaje.setVisible(true);
        tableMesaje.setDisable(false);
        tableMesaje.setItems(messages);
        txt_email.clear();
        //dataPicker1.setValue(null);
        //dataPicker2.setValue(null);

    }
    public void handleData(){
       /* LocalDate data1=dataPicker1.getValue();
        LocalDate data2=dataPicker2.getValue();
        if(data2==null||data1==null){
             AlertaBox.display("error","alege data 2 sau 1");
            return;
        }
        if(data2.isBefore(data1)){
            //sau cu text rosu afisat,in loc de pop up
            AlertaBox.display("error","ordinea cronologica!!!!!!!");
            //dataPicker2.setValue(null);
            return;
        }*/
    }
    /**
     *ia datele din pickeruri ,afiseaza mesaje primite in perioada data si prieteni facuti in perioada data
     */
    public void handleActivitate(){
        LocalDate data1=dataPicker1.getValue();
        if(data1==null){
            AlertaBox.display("error","selectati o data");
            return;
        }
        LocalDate data2=dataPicker2.getValue();
        if(data2==null){
            AlertaBox.display("error","selectati data");
            return;
        }

        if(data2.isBefore(data1)||data1.equals(data2)){
            AlertaBox.display("error","data1 trebuie sa fie inainte de data2");
            return;
        }

        List<Message>ptmesaje=StreamSupport.stream(ui.messageService.conversatie_perioada(curent.getId(),data1,data2).spliterator(),false)
                .collect(Collectors.toList());
        List<Utilizator>ptprieteni=StreamSupport.stream(ui.utilizatorService.init_prieteni_perioada(curent.getId(),data1,data2).spliterator(),false)
                .collect(Collectors.toList());

        l_friends.setText("Friends");
        //am listele,setez in tabel
        tableColumnFrom.setCellValueFactory(param->new ReadOnlyObjectWrapper<String>(ui.utilizatorService.findId(param.getValue().getId_from()).getFirstName()));

        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<Message,String>("mesaj"));
        tableColumnReply.setCellValueFactory(param -> new ReadOnlyObjectWrapper<String>(param.getValue().toStringReply()));
        tableMesaje.setEditable(true);
        tableMesaje.setVisible(true);
        tableMesaje.setDisable(false);
        friends.setAll(ptprieteni);
        messages.setAll(ptmesaje);
        tableMesaje.setItems(messages);
        initTableUseri();
        paginator.setMaxPageIndicatorCount(1);

        if(friends.size()<=5)
            paginator.setPageCount(1);
        else{
            if(friends.size()%5==0)
                paginator.setPageCount(friends.size()/5);
            else
                paginator.setPageCount(friends.size()/5+1);
        }
        paginator.setPageFactory(this::createPage);

        //dataPicker2.setValue(null);
        //dataPicker1.setValue(null);
    }

    /**
     * fct ce incarca in tabel in pdf conversatia cu userul selectat
     * @throws DocumentException
     * @throws FileNotFoundException
     */
    public void handlePdfConv()throws DocumentException,FileNotFoundException {
        LocalDate data1=dataPicker1.getValue();
        if(data1==null){
            AlertaBox.display("error","selectati o data");
            return;
        }
        LocalDate data2=dataPicker2.getValue();
        if(data2==null){
            AlertaBox.display("error","selectati data");
            return;
        }
        if(data2.isBefore(data1)||data1.equals(data2)){
            AlertaBox.display("error","data1 trebuie sa fie inainte de data2");
            return;
        }
        Utilizator selectedFriend=tableUtilizatori.getSelectionModel().getSelectedItem();
        if(selectedFriend==null){
            AlertaBox.display("error","va rugam selectati un user");
            return;
        }
        List<Message>ptmesaje=StreamSupport.stream(ui.messageService.conversatie_cu(selectedFriend.getId(),curent.getId(),data1,data2).spliterator(),false)
                .collect(Collectors.toList());
        messages.setAll(ptmesaje);
        //am setat  listele,setez tabelele
        init();//tabel 1 setat
        tableColumnFrom.setCellValueFactory(param->new ReadOnlyObjectWrapper<String>(ui.utilizatorService.findId(param.getValue().getId_from()).getFirstName()));
        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<Message,String>("mesaj"));
        tableColumnReply.setCellValueFactory(param -> new ReadOnlyObjectWrapper<String>(param.getValue().toStringReply()));
        tableMesaje.setEditable(true);
        tableMesaje.setVisible(true);
        tableMesaje.setDisable(false);
        tableMesaje.setItems(messages);
        txt_email.clear();
        dataPicker1.setValue(null);
        dataPicker2.setValue(null);

        JFrame parentComponent=new JFrame();
        JFileChooser fileChooser=new JFileChooser();
        int returnval=fileChooser.showOpenDialog(parentComponent);
        if(returnval==JFileChooser.APPROVE_OPTION){
            File filetosave=fileChooser.getSelectedFile();
            try{
                Document document=new Document();
                PdfWriter writer=PdfWriter.getInstance(document,new FileOutputStream(filetosave));
                document.open();
                //scriu in el
                Paragraph titlu=new Paragraph("              Conversatiile avute cu "+selectedFriend.getFirstName()+" "+selectedFriend.getLastName()+" in perioada "+data1.toString()+"/"+data2.toString());
                Paragraph into=new Paragraph("Conversatii");
                Paragraph isspace=new Paragraph(" ");

                //creez tabel
                PdfPTable table=new PdfPTable(5);
                //3=nr col
                PdfPCell nume1=new PdfPCell(new Paragraph("FIRSTNAME"));
                PdfPCell nume2=new PdfPCell(new Paragraph("LASTNAME"));
                PdfPCell mesaj=new PdfPCell(new Paragraph("MESSAGE"));
                PdfPCell reply=new PdfPCell(new Paragraph("REPLY"));
                PdfPCell data_msg=new PdfPCell(new Paragraph("DATE"));
                table.addCell(nume1);
                table.addCell(nume2);
                table.addCell(mesaj);
                table.addCell(reply);
                table.addCell(data_msg);
                //creez celule
                for(Message msg:ptmesaje)
                {   PdfPCell c1=new PdfPCell(new Paragraph(ui.utilizatorService.findId(msg.getId_from()).getFirstName()));
                    PdfPCell c2=new PdfPCell(new Paragraph(ui.utilizatorService.findId(msg.getId_from()).getLastName()));
                    PdfPCell c3=new PdfPCell(new Paragraph(msg.getMesaj()));
                    PdfPCell c4;
                    if(msg.getReply()==null){
                        c4=new PdfPCell(new Paragraph(" "));
                    }
                    else
                    {c4=new PdfPCell(new Paragraph(msg.toStringReply()));}
                    PdfPCell c5=new PdfPCell(new Paragraph(msg.getData().toLocalDate().toString()));
                    table.addCell(c1);
                    table.addCell(c2);
                    table.addCell(c3);
                    table.addCell(c4);
                    table.addCell(c5);
                }


                //now add the objects to the document
                document.add(titlu);
                document.add(isspace);
                document.add(into);
                document.add(isspace);
                document.add(table);

                document.close();
                JOptionPane.showMessageDialog(null,"pdf salvat");

            }catch(Exception e){
                JOptionPane.showMessageDialog(null,e);
            }
        }



    }

    /**
     * fct ce salveaza in pdf activitatea din perioada data
     * @throws DocumentException
     * @throws FileNotFoundException
     */
    public void handlePdfAct()throws DocumentException,FileNotFoundException{
        LocalDate data1=dataPicker1.getValue();
        if(data1==null){
            AlertaBox.display("error","selectati o data");
            return;
        }
        LocalDate data2=dataPicker2.getValue();
        if(data2==null){
            AlertaBox.display("error","selectati data");
            return;
        }
        if(data2.isBefore(data1)||data1.equals(data2)){
            AlertaBox.display("error","data1 trebuie sa fie inainte de data2");
            return;
        }


        List<Message>ptmesaje=StreamSupport.stream(ui.messageService.conversatie_perioada(curent.getId(),data1,data2).spliterator(),false)
                .collect(Collectors.toList());
        List<Utilizator>ptprieteni=StreamSupport.stream(ui.utilizatorService.init_prieteni_perioada(curent.getId(),data1,data2).spliterator(),false)
                .collect(Collectors.toList());

        l_friends.setText("Friends");
        //am listele,setez in tabel
        tableColumnFrom.setCellValueFactory(param->new ReadOnlyObjectWrapper<String>(ui.utilizatorService.findId(param.getValue().getId_from()).getFirstName()));

        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<Message,String>("mesaj"));
        tableColumnReply.setCellValueFactory(param -> new ReadOnlyObjectWrapper<String>(param.getValue().toStringReply()));
        tableMesaje.setEditable(true);
        tableMesaje.setVisible(true);
        tableMesaje.setDisable(false);
        friends.setAll(ptprieteni);
        messages.setAll(ptmesaje);
        tableMesaje.setItems(messages);
        initTableUseri();
        paginator.setMaxPageIndicatorCount(1);

        if(friends.size()<=5)
            paginator.setPageCount(1);
        else{
            if(friends.size()%5==0)
                paginator.setPageCount(friends.size()/5);
            else
                paginator.setPageCount(friends.size()/5+1);
        }
        paginator.setPageFactory(this::createPage);

        dataPicker2.setValue(null);
        dataPicker1.setValue(null);
        //acum pun si in pdf


        JFrame parentComponent=new JFrame();
        JFileChooser fileChooser=new JFileChooser();
        int returnval=fileChooser.showOpenDialog(parentComponent);
        if(returnval==JFileChooser.APPROVE_OPTION){
            File filetosave=fileChooser.getSelectedFile();
            try{
                Document document=new Document();
                PdfWriter writer=PdfWriter.getInstance(document,new FileOutputStream(filetosave));
                document.open();
                //scriu in el
                Paragraph titlu=new Paragraph("                      Activitatea dumneavoastra din perioada "+data1.toString()+"/"+data2.toString());
                Paragraph into=new Paragraph("Conversatii");
                Paragraph into2=new Paragraph("Prieteni noi");
                Paragraph isspace=new Paragraph(" ");

                //creez tabel
                PdfPTable table=new PdfPTable(5);
                PdfPTable table2=new PdfPTable(3);
                //4,5=nr col

                //creez celule
                PdfPCell nume1=new PdfPCell(new Paragraph("FIRSTNAME"));
                PdfPCell nume2=new PdfPCell(new Paragraph("LASTNAME"));
                PdfPCell mesaj=new PdfPCell(new Paragraph("MESSAGE"));
                PdfPCell reply=new PdfPCell(new Paragraph("REPLY"));
                PdfPCell data=new PdfPCell(new Paragraph("DATE"));
                table.addCell(nume1);
                table.addCell(nume2);
                table.addCell(mesaj);
                table.addCell(reply);
                table.addCell(data);
               // PdfPCell DATE=new PdfPCell(new Paragraph("DATE"));
                PdfPCell nume1fr=new PdfPCell(new Paragraph("FIRSTNAME"));
                PdfPCell nume2fr=new PdfPCell(new Paragraph("LASTNAME"));
                PdfPCell email=new PdfPCell(new Paragraph("EMAIL"));
                table2.addCell(nume1fr);
                table2.addCell(nume2fr);
                table2.addCell(email);

                for(Message msg:ptmesaje)
                {   //first,last,mesaj,reply,data
                    PdfPCell c1=new PdfPCell(new Paragraph(ui.utilizatorService.findId(msg.getId_from()).getFirstName()));
                    PdfPCell c2=new PdfPCell(new Paragraph(ui.utilizatorService.findId(msg.getId_from()).getLastName()));
                    PdfPCell c3=new PdfPCell(new Paragraph(msg.getMesaj()));
                    PdfPCell c4;
                    if(msg.getReply()==null)
                        c4=new PdfPCell(new Paragraph(" "));
                    else
                        c4=new PdfPCell(new Paragraph(msg.toStringReply()));
                    PdfPCell c5=new PdfPCell(new Paragraph(msg.getData().toLocalDate().toString()));
                    table.addCell(c1);
                    table.addCell(c2);
                    table.addCell(c3);
                    table.addCell(c4);
                    table.addCell(c5);}

                for(Utilizator u:ptprieteni){
                    PdfPCell c1=new PdfPCell(new Paragraph(u.getFirstName()));
                    PdfPCell c2=new PdfPCell(new Paragraph(u.getLastName()));
                    PdfPCell c3=new PdfPCell(new Paragraph(u.getEmail()));
                    table2.addCell(c1);
                    table2.addCell(c2);
                    table2.addCell(c3);
                }


                //now add the objects to the document

                document.add(titlu);
                document.add(isspace);
                document.add(into);
                document.add(isspace);
                document.add(table);
                document.add(isspace);
                document.add(into2);
                document.add(isspace);
                document.add(table2);


                document.close();
                JOptionPane.showMessageDialog(null,"pdf salvat");

            }catch(Exception e){
                JOptionPane.showMessageDialog(null,e);
            }
        }
    }

}
