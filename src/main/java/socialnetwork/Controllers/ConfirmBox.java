package socialnetwork.Controllers;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmBox {
    static boolean answer;
    public static boolean display(String title,String mesaj){
        Stage window2=new Stage();
        window2.initModality(Modality.APPLICATION_MODAL);
        window2.setTitle(title);
        window2.setMinHeight(250);
        window2.setMinWidth(200);
        Label label=new Label();
        label.setText(mesaj);
        label.styleProperty().set("-fx-font-size: 1.3em");
        label.styleProperty().set("-fx-font-weight: bold italic");

        Button yesbtn=new Button("yes");
        Button nobtn=new Button("no");
        yesbtn.setOnAction(e->{
            answer=true;
            window2.close();
        });
        nobtn.setOnAction(e->{
            answer=false;
            window2.close();
        });

        VBox layout=new VBox(10);
        layout.getChildren().addAll(label,yesbtn,nobtn);
        layout.setAlignment(Pos.CENTER);
       // layout.styleProperty().set("-fx-background-color: #0099FF");

        Scene scene=new Scene(layout);
        scene.getStylesheets().add("/StyleMain.css");
        window2.setScene(scene);
        window2.showAndWait();

        return answer;
    }
}
