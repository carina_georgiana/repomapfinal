package socialnetwork.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.Event;
import socialnetwork.domain.EventDto;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.EventService;
import socialnetwork.ui.UI;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class NotificariCtrl {
    @FXML
    Button btn_close;
    @FXML
    ListView<EventDto> list_events;
    @FXML
    Label l_titlu;
    UI ui;
    EventService eventService;
    Utilizator curent;
    ObservableList<EventDto> events= FXCollections.observableArrayList();
    //int flag=1;

    public void handleClose(ActionEvent event){
        try{
            FXMLLoader loginParent=new FXMLLoader();
            loginParent.setLocation(getClass().getResource("/UserCurent.fxml"));
            AnchorPane loginLayout=(AnchorPane)loginParent.load();
            Stage window=(Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(new Scene(loginLayout));
            UserCurentCtrl loginCtrl=loginParent.getController();//seteaza controller pt fxml
            loginCtrl.ui=ui;
            loginCtrl.selectedUser=curent;
            loginCtrl.eventService=eventService;
            loginCtrl.flag=0;
            loginCtrl.init();
            window.show();}catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * fct ce incarca lista cu mesaje pentru evenimentele viitoare la care doresc notificari
     */
    public void init(){
        Iterable<Event>all=eventService.notifica_pentru(curent.getId());
        List<Event> list= StreamSupport.stream(all.spliterator(),false)
                .collect(Collectors.toList());

        List<EventDto>listDto=new ArrayList<>();
        for(Event e:list){
            long nr = ChronoUnit.DAYS.between(LocalDate.now(), e.getDate());
            int nr_zile=(int)nr;
            if(nr_zile>0)
            {EventDto eventDto=new EventDto(e.getId(),e.getId_user(),e.getTitlu(),e.getDescriere(),e.getDate(),nr_zile);
            listDto.add(eventDto);}
        }
        events.setAll(listDto);
        list_events.getItems().setAll(events);
    }
}

