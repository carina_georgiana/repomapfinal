package socialnetwork.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.BCrypt.MyCrypt;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.EventService;
import socialnetwork.ui.UI;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SignInCtrl {
    //momentan e fara parola
    UI ui;
    EventService eventService;
    @FXML
    Label l_password;
    @FXML
    Label l_hello;
    @FXML
    Label l_firstName;
    @FXML
    Label l_lastName;
    @FXML
    Label l_email2;
    @FXML
    Button btn_signin;
    @FXML
    TextField txt_email;
    @FXML
    TextField txt_firstName;
    @FXML
    TextField txt_lastName;
    @FXML
    TextField txt_password;

    /**
     * fct care creeaza user nou si ii deschide si pagina curenta
     * @param event
     * @throws IOException
     */
    public void handleSignin(ActionEvent event)throws IOException {
        if(txt_email.getText().isEmpty()||txt_firstName.getText().isEmpty()||txt_lastName.getText().isEmpty()){
            AlertaBox.display("error","va rugam introduceti date");
            return;
        }
        Utilizator user=ui.utilizatorService.findByEmail(txt_email.getText());
        if(user!=null){
            AlertaBox.display("atentie","email existent!");
            txt_email.clear();
            return;
        }
        Utilizator u=new Utilizator(txt_firstName.getText(),txt_lastName.getText(),txt_email.getText(), MyCrypt.hash(txt_password.getText()));
        ui.utilizatorService.addUtilizator(u);//u nu are id,i se da in repo,din bd!!!
        Utilizator ut=ui.utilizatorService.findByEmail(txt_email.getText());//ca sa aiba si id curentul user
        AlertaBox.display("succes","utilizator adaugat");
        //deschid fereastra pt user curent
        FXMLLoader loginParent = new FXMLLoader();
        loginParent.setLocation(getClass().getResource("/UserCurent.fxml"));
        AnchorPane loginLayout = (AnchorPane) loginParent.load();
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(new Scene(loginLayout));
        UserCurentCtrl loginCtrl = loginParent.getController();
        loginCtrl.ui = ui;
        loginCtrl.selectedUser = ut;
        loginCtrl.eventService=eventService;
        loginCtrl.init();
        window.show();
    }
}
