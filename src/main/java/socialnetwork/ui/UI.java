package socialnetwork.ui;

import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;

import java.time.LocalDateTime;
import java.util.*;

public class UI {
    public Utilizator curent=new Utilizator("first","input","email@gmail.com","eusuntdemo");
    public UtilizatorService utilizatorService;
    public PrietenieService prietenieService;
    public MessageService messageService;
    public Long id_max;
    public UI(UtilizatorService u,PrietenieService p,MessageService m){
        this.utilizatorService=u;
        this.prietenieService=p;
        this.messageService=m;
        id_max=0L;
    }




    /**
     * afiseaza prietenii unui user curent,adica doar cei cu prietenii accepted
     * @param id2
     * @return
     */
    public void sendFriendRequestUser(Long id2){
        Utilizator u2 = utilizatorService.findId(id2);
        if(u2==null)
        {
            System.out.println("utilizator inexistent");
            return;
        }
        Tuple<Long,Long> tuple=new Tuple<>(curent.getId(), id2);
        Tuple<Long,Long>tuple2=new Tuple<>(id2,curent.getId());
        //obs:daca prietenia a fost refuzata o data,nu mai pot trimite->previn raportul
        if(prietenieService.findId(tuple)!=null||prietenieService.findId(tuple2)!=null){
            System.out.println("prietenie existenta");
            return;
        }
        Prietenie p=new Prietenie(curent.getId(),id2, LocalDateTime.now());
        prietenieService.save(p);
    }

    public ObservableList<Prietenie> friendsForCurent(Long id){
        List<Prietenie>fr=new ArrayList<>();
        prietenieService.getAll().forEach(fr::add);

        fr.stream()
                .filter(x->x.getStatus()!=null&&x.getStatus().equals("accepted"))
                .filter(x->x.getId().getLeft().equals(id))
                .forEach(x->{
                    Utilizator f=utilizatorService.findId(x.getId().getRight());
                    System.out.println(f.getLastName()+"|"+f.getFirstName()+"|"+x.getDate());
                });
        fr.stream()
                .filter(x->x.getStatus()!=null&&x.getStatus().equals("accepted"))
                .filter(x->x.getId().getRight().equals(id))
                .forEach(x->{
                    Utilizator f=utilizatorService.findId(x.getId().getLeft());
                    System.out.println(f.getLastName()+"|"+f.getFirstName()+"|"+x.getDate());
                });
        return null;
    }
    public List<Prietenie> showFriendsRequest(Long id){
        List<Prietenie>fr=new ArrayList<>();
        prietenieService.getAll().forEach(fr::add);
        fr.stream()
                .filter(x->x.getStatus()!=null && x.getStatus().equals("pending"))
                .filter(x->x.getId().getLeft().equals(id))
                .forEach(x->{
                    String nume=utilizatorService.findId(x.getId().getRight()).getFirstName();
                    String prenume=utilizatorService.findId(x.getId().getRight()).getLastName();
                    System.out.println(nume+" "+prenume+" "+x.getDate());
                });
        fr.stream()
                .filter(x->x.getStatus()!=null && x.getStatus().equals("pending"))
                .filter(x->x.getId().getRight().equals(id))
                .forEach(x->{
                    String nume2=utilizatorService.findId(x.getId().getRight()).getFirstName();
                    String prenume2=utilizatorService.findId(x.getId().getRight()).getLastName();
                    System.out.println(nume2+" "+prenume2+" "+x.getDate());
                });
        return fr;//aici mai trebuie lucrat,ca stream nu mi modifica lista
    }
    public void addPrietenCurent(Long id2) {
        try {
            Prietenie friendship = prietenieService.addPrietenie(curent.getId(), id2);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
public void removePrietenCurent(Long id) {
    try {
        Prietenie friendship = prietenieService.removePrietenie(curent.getId(), id);
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
}

}
