package socialnetwork.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.Controllers.ConfirmBox;
import socialnetwork.Controllers.LoginCtrl;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.EventsDB_Repo;
import socialnetwork.repository.database.MessagesDB_Repo;
import socialnetwork.repository.database.PrieteniiDB_Repo;
import socialnetwork.repository.database.UtilizatorDB_Repo;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.EventService;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;

import java.io.IOException;
public class MainApp extends Application {
    public static void main(String[] args) {
        launch(args);

    }
    //Repository<Long, Utilizator> userFileRepository ;
    PagingRepository<Long, Utilizator> userFileRepository ;
    PagingRepository<Tuple<Long, Long>, Prietenie> friendshipFileRepository;
    Repository<Long, Message>messageFileRepository;
    PagingRepository<Long, Event>eventRepository;

    UtilizatorService userService;
    PrietenieService friendshipService;
    MessageService messageService;
    EventService eventService;

    UI ui;

    @Override
    public void start(Stage primaryStage) throws IOException {

        String url= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        String username= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        String password= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.pasword");

        userFileRepository=new UtilizatorDB_Repo(url,username,password,new UtilizatorValidator());
        friendshipFileRepository=new PrieteniiDB_Repo(url,username,password,new PrietenieValidator());
        messageFileRepository=new MessagesDB_Repo(url, username, password, new MessageValidator());
        eventRepository=new EventsDB_Repo(url, username, password, new EventValidator());

         userService = new UtilizatorService(userFileRepository, friendshipFileRepository);
         messageService=new MessageService(messageFileRepository);
         eventService=new EventService(eventRepository);
        friendshipService = new PrietenieService(userFileRepository, friendshipFileRepository);

        //userService.setPageSize(2);

        ui = new UI(userService, friendshipService,messageService);

        initView(primaryStage);
        primaryStage.setWidth(800);
        primaryStage.setTitle("SocialTime");
        primaryStage.setOnCloseRequest(e->{
            e.consume();
            Boolean answer= ConfirmBox.display("inchidere","esti sigur?");
            if(answer==true){
                System.out.println("da");
                primaryStage.close();}
        });
        primaryStage.sizeToScene();//pt a se redimensiona automat
        primaryStage.show();


    }

    private void initView(Stage primaryStage) throws IOException {

        FXMLLoader prieteniLoader = new FXMLLoader();
        prieteniLoader.setLocation(getClass().getResource("/LoginFxml.fxml"));
        AnchorPane prieteniLayout=prieteniLoader.load();
        primaryStage.setScene(new Scene(prieteniLayout));//seteaza scena

        LoginCtrl lgCtrl=prieteniLoader.getController();
        lgCtrl.ui=ui;
        lgCtrl.eventService=eventService;
        lgCtrl.init();

    }
}
