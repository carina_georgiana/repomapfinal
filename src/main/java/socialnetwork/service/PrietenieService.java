package socialnetwork.service;

import socialnetwork.Controllers.Observable;
import socialnetwork.Controllers.Observer;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PrietenieService implements Observable {
    private PagingRepository<Long, Utilizator> repoUtilizatori;
    private PagingRepository<Tuple<Long,Long>, Prietenie>repoPrietenii;

    public final String url="jdbc:postgresql://localhost:5432/SocialNetwork";
    public final String username="postgres";
    public final String password= "negruzzi";

    public Prietenie findId(Tuple<Long,Long> id){
        return repoPrietenii.findOne(id);
    }

    public Prietenie save(Prietenie p){return repoPrietenii.save(p);}

    public Prietenie delete(Tuple<Long,Long>id){return repoPrietenii.delete(id);}

    public PrietenieService(PagingRepository<Long, Utilizator> repo1,PagingRepository<Tuple<Long,Long>,Prietenie>repo2) {

        this.repoUtilizatori = repo1;
        this.repoPrietenii=repo2;
        if(repoPrietenii.findAll()!=null)
        {
            List<Prietenie> friendships= StreamSupport.stream(repoPrietenii.findAll().spliterator(),false)
                .collect(Collectors.toList());
        for(Prietenie p:friendships)
           {
          if(p.getStatus().equals("accepted"))
          {Long id1=p.getId().getLeft();
            Long id2=p.getId().getRight();
            Utilizator u1=repoUtilizatori.findOne(id1);
            Utilizator u2=repoUtilizatori.findOne(id2);
            u1.addPrieten(u2);
            u2.addPrieten(u1);}
        }

        }
    }
    public Iterable<Prietenie>getAll(){return repoPrietenii.findAll();}
    /**
     *
     * @param id1
     * @param id2-id urile celor 2 prieteni
     * @return-prietenie formata
     * @throws ValidationException daca id invalid
     */
    public Prietenie addPrietenie(Long id1, Long id2) {
        Prietenie friendship = new Prietenie(id1,id2, LocalDateTime.now());
        if(repoPrietenii.findAll()!=null)
        {
            List<Prietenie> friendships=StreamSupport.stream(repoPrietenii.findAll().spliterator(),false)
                    .collect(Collectors.toList());
            for(Prietenie p:friendships){
                Long idLista1=p.getId().getLeft();
                Long idLista2=p.getId().getRight();
                if(idLista1.equals(id1)&&idLista2.equals(id2))
                    throw new ValidationException("prietenia exista deja");
                if(idLista1.equals(id2)&&idLista2.equals(id1))
                    throw new ValidationException("prietenia exista deja,in ordinea id2,id1");

            }
        }
        Prietenie task = repoPrietenii.save(friendship);
        return task;
    }
    /**
     *
     * @param id1
     * @param id2-cei 2 prieteni ce compun prietenia
     * @return-prietenia stearsa
     * @throws ValidationException-daca id invalid
     */
    public Prietenie removePrietenie(Long id1, Long id2){

        Tuple idf1 = new Tuple<Long, Long>(id1, id2);
        Tuple idf2 = new Tuple<Long, Long>(id2, id1);
        Utilizator u1 = repoUtilizatori.findOne(id1);
        Utilizator u2 = repoUtilizatori.findOne(id2);
        if(repoPrietenii.findOne(idf1)!=null){
            Prietenie task1 = repoPrietenii.delete(idf1);
            notifyObservers();
            return task1; }
        else{
            //daca ajunge aici inseamna ca a gasit cu idf2
            Prietenie task2 = repoPrietenii.delete(idf2);
            notifyObservers();
            return task2;
        }

    }

    /**
     *
     * @param aLong-id user
     * @return-lista prieteniilor unui user cu id=aLong
     */
    public Iterable<Prietenie>my_friendships(Long aLong){
        Set<Prietenie> prietenii = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"'+" WHERE "+"("+'"'+"Prietenii"+'"'+".id2=?"+" or "+'"'+"Prietenii"+'"'+".id1=?"+")"))
        {
            statement.setLong(1,aLong);
            statement.setLong(2,aLong);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                //construiesc prietenie
                String status=resultSet.getString("status");
                Timestamp data=resultSet.getTimestamp("date");
                LocalDateTime date=data.toLocalDateTime();
                Prietenie p=new Prietenie(id1,id2,date,status);
                prietenii.add(p);

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return prietenii;
    }
    private List<Observer>observers=new ArrayList<>();
    @Override
    public void addObserver(Observer e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers() {
        observers.stream().forEach(x->x.update());
    }

    private int page = 0;
    private int size = 5;


    public int getPage(){return page;}
    public List<Prietenie>getUsersOnPage(int page){
        this.page=page;
        Pageable pageable=new PageableImplementation(page,this.size);
        Page<Prietenie> userPage=repoPrietenii.findAll(pageable);
        return userPage.getContent().collect(Collectors.toList());
    }
    public List<Prietenie> getNextUsersOnPage(){
        this.page++;
        return getUsersOnPage(this.page);
    }

}
