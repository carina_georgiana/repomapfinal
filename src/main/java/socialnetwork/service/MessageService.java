package socialnetwork.service;

import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.MessageFile;

import java.sql.*;
import java.time.LocalDate;
import java.util.*;

public class MessageService {
    private Repository<Long, Message> repoMesaje;
    public final String url="jdbc:postgresql://localhost:5432/SocialNetwork";
    public final String username="postgres";
    public final String password= "negruzzi";

    public MessageService(Repository<Long, Message> r) {
        this.repoMesaje = r;
    }

    public Message save(Message m) {
        return repoMesaje.save(m);
    }

    public Message delete(Long id){return repoMesaje.delete(id);}


    public Iterable<Message> getAll() {
        return repoMesaje.findAll();
    }

    public Message findId(Long id) {
        return repoMesaje.findOne(id);
    }

    /**
     *
     * @param id_cu-id user
     * @param my_id-id user
     * @param data1
     * @param data2
     * @return-lista cronologica a mesajelor din perioada data,dintre user1 si user2-fct utila la rapoarte
     */
    public Iterable<Message>conversatie_cu(Long id_cu, Long my_id, LocalDate data1,LocalDate data2){
        List<Message> messages = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Messages"+'"'+" WHERE"+" ("+'"'+"to"+'"'+" like ? or "+'"'+"to"+'"'+" like ? or "+'"'+"to"+'"'+" like ? or "+'"'+"to"+'"'+" like ?"+") "+"AND "+'"'+"from"+'"'+"=?"+" ORDER BY id"))
        {
            statement.setString(1, "%,"+my_id+",%");
            statement.setString(2, "%,"+my_id);
            statement.setString(3, my_id+",%");
            statement.setString(4, my_id.toString());

            //AND date>? AND date<?
           // statement.setTimestamp(6, Timestamp.valueOf(data1.atStartOfDay()));
            //statement.setTimestamp(7, Timestamp.valueOf(data1.atTime(23,30)));
            statement.setLong(5,id_cu);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String to1 = resultSet.getString("to");
                String message1= resultSet.getString("message");
                Timestamp date1=resultSet.getTimestamp("date");
                Long reply=resultSet.getLong("idReply");

                if(date1.toLocalDateTime().isBefore(data2.atStartOfDay())&&date1.toLocalDateTime().isAfter(data1.atTime(23,30))){
                List<String> list_to_String= Arrays.asList(to1.split(","));

                List<Long>list_to_ids=new ArrayList<>();
                for(String tos:list_to_String)
                    list_to_ids.add(Long.parseLong(tos));

                if(reply==null) {
                    Message message = new Message(id_cu,list_to_ids,message1);
                    message.setId(id);
                    message.setData(date1.toLocalDateTime());
                    messages.add(message);
                }
                else{
                    Message replyMessage=findId(reply);
                    Message message = new Message(id_cu,list_to_ids,message1,replyMessage);
                    message.setId(id);
                    message.setData(date1.toLocalDateTime());
                    messages.add(message);
                }}
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }

    /**
     *
     * @param my_id
     * @param data1
     * @param data2
     * @return-lista de mesaje a userului cu id=my_id din perioada data
     */
    public Iterable<Message>conversatie_perioada(Long my_id, LocalDate data1,LocalDate data2){
        List<Message> messages = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Messages"+'"'+" WHERE"+" ("+'"'+"to"+'"'+" like ? or "+'"'+"to"+'"'+" like ? or "+'"'+"to"+'"'+" like ? or "+'"'+"to"+'"'+" like ?"+")"+"ORDER BY id"))
        {
            statement.setString(1, "%,"+my_id+",%");
            statement.setString(2, "%,"+my_id);
            statement.setString(3, my_id+",%");
            statement.setString(4, my_id.toString());

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String to1 = resultSet.getString("to");
                String message1= resultSet.getString("message");
                Timestamp date1=resultSet.getTimestamp("date");
                Long from=resultSet.getLong("from");
                Long reply=resultSet.getLong("idReply");

                if(date1.toLocalDateTime().isBefore(data2.atStartOfDay())&&date1.toLocalDateTime().isAfter(data1.atTime(23,30))){
                    List<String> list_to_String= Arrays.asList(to1.split(","));

                    List<Long>list_to_ids=new ArrayList<>();
                    for(String tos:list_to_String)
                        list_to_ids.add(Long.parseLong(tos));

                    if(reply==null) {
                        Message message = new Message(from,list_to_ids,message1);
                        message.setId(id);
                        message.setData(date1.toLocalDateTime());
                        messages.add(message);
                    }
                    else{
                        Message replyMessage=findId(reply);
                        Message message = new Message(from,list_to_ids,message1,replyMessage);
                        message.setId(id);
                        message.setData(date1.toLocalDateTime());
                        messages.add(message);
                    }}
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }

}