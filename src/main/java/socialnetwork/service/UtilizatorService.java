package socialnetwork.service;

import socialnetwork.domain.Event;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UtilizatorService {
    private PagingRepository<Long, Utilizator> repoUtilizatori;
    private PagingRepository<Tuple<Long, Long>, Prietenie> repoPrietenii;

    public final String url="jdbc:postgresql://localhost:5432/SocialNetwork";
    public final String username="postgres";
    public final String password= "negruzzi";

    public Utilizator findId(Long id) {
        return repoUtilizatori.findOne(id);
    }

    public UtilizatorService(PagingRepository<Long, Utilizator> repo1, PagingRepository<Tuple<Long, Long>, Prietenie> repo2) {
        this.repoUtilizatori = repo1;
        this.repoPrietenii = repo2;
    }
    /**
     * @param messageTask-User de adaugat
     * @return
     */
    public Utilizator addUtilizator(Utilizator messageTask) {
        Utilizator task = repoUtilizatori.save(messageTask);
        //verificare sa nu existe deja si validare cu validator
        return task;
    }

    /**
     * @param id-sterge user dupa id
     * @throws ValidationException-daca id invalid
     * @return-user sters
     */
    public Utilizator removeUtilizator(Long id) {

        Utilizator user = repoUtilizatori.findOne(id);
        if (user == null)
            throw new ValidationException("id invalid\n");
        Iterable<Prietenie> friends = repoPrietenii.findAll();
        List<Prietenie> friendships = new ArrayList<>();
        for (Prietenie fr : friends) {
            friendships.add(fr);
        }
        for (Prietenie fr : friendships) {
            if (fr.getId().getLeft().equals(id)) {
                Long id2 = fr.getId().getRight();
                repoPrietenii.delete(new Tuple<>(id, id2));
            }
            if (fr.getId().getRight().equals(id)) {
                Long id2 = fr.getId().getLeft();
                repoPrietenii.delete(new Tuple<>(id2, id));
            }
        }
        Utilizator task = repoUtilizatori.delete(id);

        return task;
    }

    public Iterable<Utilizator> getAll() {
        return repoUtilizatori.findAll();
    }

    /**
     *
     * @param cautat
     * @return-lista cu utilizatorii cautati dupa string ul parametru
     */
    public Iterable<Utilizator>getSearch(String cautat){
        Set<Utilizator> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Utilizatori"+'"'+" WHERE email like ? or "+'"'+"firstName"+'"'+" like ? or "+'"'+"lastName"+'"'+" like ?"))
        {
            statement.setString(1, cautat+"%");
            statement.setString(2, cautat+"%");
            statement.setString(3, cautat+"%");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String email=resultSet.getString("email");
                String password=resultSet.getString("password");
                Utilizator utilizator = new Utilizator(firstName, lastName,email,password);
                utilizator.setId(id);

                users.add(utilizator);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    /**
     *
     * @param aLong-id ul unui user
     * @return-lista de prieteni ai userului dat
     */
    public Iterable<Utilizator>init_prieteni(Long aLong){
        Set<Utilizator> prieteni = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"'+" inner join "+'"'+"Utilizatori"+'"'+"on"+'"'+"Prietenii"+'"'+".id1="+'"'+"Utilizatori"+'"'+".id "+" WHERE "+'"'+"Prietenii"+'"'+".id2=?"+"AND"+'"'+"Prietenii"+'"'+".status=?"))
        {
            statement.setLong(1,aLong);
            statement.setString(2,"accepted");
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                //construiesc user
                String firstName=resultSet.getString("firstName");
                String lastName=resultSet.getString("lastName");
                String email=resultSet.getString("email");
                String password=resultSet.getString("password");
               // Utilizator friend=new Utilizator(id1,firstName,lastName,email,"eusunt"+id1);
                Utilizator friend=new Utilizator(id1,firstName,lastName,email,password);
                prieteni.add(friend);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"'+" inner join "+'"'+"Utilizatori"+'"'+"on"+'"'+"Prietenii"+'"'+".id2="+'"'+"Utilizatori"+'"'+".id "+" WHERE "+'"'+"Prietenii"+'"'+".id1=?"+"AND"+'"'+"Prietenii"+'"'+".status=?"))
        {
            statement.setLong(1,aLong);
            statement.setString(2,"accepted");
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id2 = resultSet.getLong("id2");
                //construiesc user
                String firstName=resultSet.getString("firstName");
                String lastName=resultSet.getString("lastName");
                String email=resultSet.getString("email");
                String password=resultSet.getString("password");
                //Utilizator friend=new Utilizator(id2,firstName,lastName,email,"eusunt"+id2);
                Utilizator friend=new Utilizator(id2,firstName,lastName,email,password);
                prieteni.add(friend);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return prieteni;
    }

    /**
     *
     * @param aLong-id user
     * @param data1
     * @param data2
     * @return-lista de prieteni facuti de userul cu id=aLong in perioada data-fct utila la rapoarte
     */
    public Iterable<Utilizator>init_prieteni_perioada(Long aLong,LocalDate data1,LocalDate data2){
        Set<Utilizator> prieteni = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"'+" inner join "+'"'+"Utilizatori"+'"'+"on"+'"'+"Prietenii"+'"'+".id1="+'"'+"Utilizatori"+'"'+".id "+" WHERE "+'"'+"Prietenii"+'"'+".id2=?"+"AND"+'"'+"Prietenii"+'"'+".status=?"))
        {
            statement.setLong(1,aLong);
            statement.setString(2,"accepted");
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                //construiesc user
                String firstName=resultSet.getString("firstName");
                String lastName=resultSet.getString("lastName");
                String email=resultSet.getString("email");
                String password=resultSet.getString("password");
                Timestamp date=resultSet.getTimestamp("date");
                if(date.toLocalDateTime().isAfter(data1.atStartOfDay())&&date.toLocalDateTime().isBefore(data2.atTime(23,30)))
                {
               // Utilizator friend=new Utilizator(id1,firstName,lastName,email,"eusunt"+id1);
                Utilizator friend=new Utilizator(id1,firstName,lastName,email,password);
                prieteni.add(friend);}
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"'+" inner join "+'"'+"Utilizatori"+'"'+"on"+'"'+"Prietenii"+'"'+".id2="+'"'+"Utilizatori"+'"'+".id "+" WHERE "+'"'+"Prietenii"+'"'+".id1=?"+"AND"+'"'+"Prietenii"+'"'+".status=?"))
        {
            statement.setLong(1,aLong);
            statement.setString(2,"accepted");
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id2 = resultSet.getLong("id2");
                //construiesc user
                String firstName=resultSet.getString("firstName");
                String lastName=resultSet.getString("lastName");
                String email=resultSet.getString("email");
                String pas=resultSet.getString("password");
                Timestamp date=resultSet.getTimestamp("date");
                if(date.toLocalDateTime().isAfter(data1.atStartOfDay())&&date.toLocalDateTime().isBefore(data2.atTime(23,30)))
                {
                    //Utilizator friend=new Utilizator(id2,firstName,lastName,email,"eusunt"+id2);
                    Utilizator friend=new Utilizator(id2,firstName,lastName,email,pas);
                    prieteni.add(friend);}

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return prieteni;
    }
    //fct ce returneaza nr total de useri
    public int getSize(){
        int nr=0;
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("SELECT COUNT(*) from public."+'"'+"Utilizatori"+'"');
            while(resultSet.next())
            {
            nr=resultSet.getInt(1);
            resultSet.close();
            statement.close();
            //connection.close();
            return nr;}

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return nr;
    }
    public Utilizator findByEmail(String email) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Utilizatori"+'"'+" WHERE email=?"))
        {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String parola=resultSet.getString("password");
                //Utilizator utilizator = new Utilizator(firstName, lastName,email,"eusunt"+id);//aici are id=null
                Utilizator utilizator = new Utilizator(firstName, lastName,email,parola);
                utilizator.setId(id);
                return utilizator;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    private int page = 0;
    private int size = 5;


    public int getPage(){return page;}
    public List<Utilizator>getUsersOnPage(int page){
        this.page=page;
        Pageable pageable=new PageableImplementation(page,this.size);
        Page<Utilizator>userPage=repoUtilizatori.findAll(pageable);
        return userPage.getContent().collect(Collectors.toList());
    }
    public List<Utilizator> getNextUsersOnPage(){
        this.page++;
        return getUsersOnPage(this.page);
    }
    public int numberOfPagesWithUsers(){
        return getSize()/size;
    }
}











   /* public void setPageSize(int size) {
        this.size = size;
    }
    public Set<Utilizator> getNextMessages() {
    this.page++;
        return getMessagesOnPage(this.page);
    }
    public Set<Utilizator> getMessagesOnPage(int page) {
        this.page=page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page<Utilizator> studentPage = repoUtilizatori.findAll(pageable);
        return studentPage.getContent().collect(Collectors.toSet());
    }*/

