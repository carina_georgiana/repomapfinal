package socialnetwork.service;

import org.postgresql.util.PSQLException;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Event;
import socialnetwork.domain.Message;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class EventService {
      PagingRepository<Long, Event> repoEvents;
    public final String url="jdbc:postgresql://localhost:5432/SocialNetwork";
    public final String username="postgres";
    public final String password= "negruzzi";
                            //ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.pasword");

    public EventService(PagingRepository<Long, Event> r) {
        this.repoEvents = r;
    }

    public Event save(Event m) {
        return repoEvents.save(m);
    }

    public Event delete(Long id){return repoEvents.delete(id);}


    public Iterable<Event> getAll() {
        return repoEvents.findAll();
    }

    public Event findId(Long id) {
        return repoEvents.findOne(id);
    }

    /**
     *
     * @param id_user
     * @param id_event
     * @return-fct va returna mereu true:se realizeaza abonarea userului cu id_user la evenimentul cu id_event
     */
    public boolean abonare(Long id_user,Long id_event) {
        if (id_event==null||id_user==null)
            throw new IllegalArgumentException("entity must be not null");
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO public."+'"'+"Events_Users"+'"'+"("+'"'+"id_user"+'"'+","+'"'+"id_event"+'"'+")" +"VALUES (?,?)"))
        {
            statement.setLong(1,id_user);
            statement.setLong(2,id_event);
            try {
                ResultSet resultSet = statement.executeQuery();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
        //mereu va da return true
    }

    //dezabonare user de la event
    public boolean dezabonare(Long id_user,Long id_event) {
        if (id_event==null||id_user==null)
            throw new IllegalArgumentException("entity must be not null");
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM public."+'"'+"Events_Users"+'"'+" WHERE id_user=? and id_event=?"))
        {
            statement.setLong(1,id_user);
            statement.setLong(2,id_event);
            try {
                ResultSet resultSet = statement.executeQuery();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
        //mereu va da return true
    }

    /**
     *
     * @param aLong
     * @return-lista de evenimente la care este abonat userul cu id=aLong
     */
    public Iterable<Event> abonat_la(Long aLong) {
        //daca id null=>exceptie
        Set<Event> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events_Users"+'"'+" WHERE id_user=?"))
        {
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("id_event");
                //construiesc e
                Event e=repoEvents.findOne(id);
                events.add(e);

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }
    //public Page<Event> findAll(Pageable pageable)
    //events create de userul cu id=aLong
    public Iterable<Event> create_de(Long aLong) {
        Set<Event> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events"+'"'+" WHERE id_user=?"))
        {
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long id_user = resultSet.getLong("id_user");
                String titlu = resultSet.getString("titlu");
                String descriere = resultSet.getString("descriere");
                Timestamp date1 = resultSet.getTimestamp("date");
                LocalDate data=date1.toLocalDateTime().toLocalDate();
                Event e=new Event(id,id_user,titlu,descriere,data);
                events.add(e);

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }

    /**
     *
     * @param id_user
     * @param id_event
     * @return
     * fct ce cauta in tabelul cu abonamente un abonament
     */
    public Tuple<Long,Long> findOneMN(Long id_user,Long id_event) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events_Users"+'"'+" WHERE id_user=? and id_event=?"))
        {
            statement.setLong(1,id_user);
            statement.setLong(2,id_event);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id_userfind = resultSet.getLong("id_user");
                Long id_eventfind = resultSet.getLong("id_event");
                Boolean notificari=resultSet.getBoolean("notificari");
                if(notificari==true){
                    return new Tuple<Long,Long>(0L,0L);
                }
                Tuple<Long,Long>t=new Tuple<>(id_userfind,id_eventfind);
                return t;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param id_u
     * @param id_e
     * fct ce seteaza flagul din baza de date a i userul cu id_u nu va mai primi notificari la evenimentul cu id_e
     */
    public void stop_notify(Long id_u,Long id_e) {
        //daca id null=>exceptie

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("UPDATE public."+'"'+"Events_Users"+'"'+"SET notificari=? "+" WHERE id_user=? AND id_event=?"))
        {
            statement.setBoolean(1,false);
            statement.setLong(2,id_u);
            statement.setLong(3,id_e);
            try {
                ResultSet resultSet = statement.executeQuery();
                //statement.executeUpdate();
            }
            catch (PSQLException e){};
            //psql prinde exceptia cand query ul nu returneaza nimic,ca aici,analog insert,delete etc
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * fct ce seteaza flagul din baza de date a i userul cu id_u  va  primi notificari la evenimentul cu id_e
     * @param id_u
     * @param id_e
     */
    public void start_notify(Long id_u,Long id_e) {
        //daca id null=>exceptie

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("UPDATE public."+'"'+"Events_Users"+'"'+"SET notificari=? "+" WHERE id_user=? AND id_event=?"))
        {
            statement.setBoolean(1,true);
            statement.setLong(2,id_u);
            statement.setLong(3,id_e);
            try {
                ResultSet resultSet = statement.executeQuery();
                //statement.executeUpdate();
            }
            catch (PSQLException e){};
            //psql prinde exceptia cand query ul nu returneaza nimic,ca aici,analog insert,delete etc
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param aLong
     * @return-evenimentele viitoare pentru care userul va primi notificari
     */
    public Iterable<Event>notificari_viitoare(Long aLong){
        Set<Event> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events"+'"'+" inner join "+'"'+"Events_Users"+'"'+"on"+'"'+"Events"+'"'+".id="+'"'+"Events_Users"+'"'+".id_event "+" WHERE "+'"'+"Events_Users"+'"'+".id_user=? AND notificari=? AND date > now()::date-1"))
        {
            statement.setLong(1,aLong);
            statement.setBoolean(2,true);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("id_event");
                //construiesc e
                Long id_user=resultSet.getLong("id_user");
                String titlu=resultSet.getString("titlu");
                String descriere=resultSet.getString("descriere");
                Timestamp data=resultSet.getTimestamp("date");
                LocalDate date=data.toLocalDateTime().toLocalDate();
                Event e=new Event(id,id_user,titlu,descriere,date);
                events.add(e);

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }
    public int nr_notificari_viitoare(Long aLong){
        int nr=0;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT count(*) from public."+'"'+"Events"+'"'+" inner join "+'"'+"Events_Users"+'"'+"on"+'"'+"Events"+'"'+".id="+'"'+"Events_Users"+'"'+".id_event "+" WHERE "+'"'+"Events_Users"+'"'+".id_user=? AND notificari=? AND date > now()::date-1"))
        {
            statement.setLong(1,aLong);
            statement.setBoolean(2,true);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                nr=resultSet.getInt(1);
                resultSet.close();
                statement.close();
                //connection.close();
                return nr;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return nr;
    }
    public Iterable<Event> notifica_pentru(Long aLong){
        //fct ce returneaza,pt un user,eveimentele  la care doreste notificari
        Set<Event> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events_Users"+'"'+" WHERE id_user=? AND notificari=?"))
        {
            statement.setLong(1,aLong);
            statement.setBoolean(2,true);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("id_event");
                //construiesc e
                Event e=repoEvents.findOne(id);
                events.add(e);

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }
    private int page = 0;
    private int size = 5;


    public int getPage(){return page;}
    public List<Event> getEventsOnPage(int page){
        this.page=page;
        Pageable pageable=new PageableImplementation(page,this.size);
        Page<Event>eventPage=repoEvents.findAll(pageable);
        return eventPage.getContent().collect(Collectors.toList());
    }
    public List<Event> getNextUsersOnPage(){
        this.page++;
        return getEventsOnPage(this.page);
    }
}
