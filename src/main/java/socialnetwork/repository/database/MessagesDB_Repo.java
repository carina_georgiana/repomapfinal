package socialnetwork.repository.database;

import org.postgresql.util.PSQLException;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

public class MessagesDB_Repo implements Repository<Long,Message> {
    private String url;
    private String username;
    private String password;
    private Validator<Message> validator;
    public MessagesDB_Repo(String url, String username, String password, Validator<Message> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public Message findOne(Long aLong) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Messages"+'"'+" WHERE id=?"))
        {
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long from1 = resultSet.getLong("from");
                String to1 = resultSet.getString("to");
                String message1 = resultSet.getString("message");
                Timestamp date1 = resultSet.getTimestamp("date");
                Long reply = resultSet.getLong("idReply");

                List<String> list_to_String = Arrays.asList(to1.split(","));

                List<Long>list_to_ids=new ArrayList<>();
                for(String ids:list_to_String)
                    list_to_ids.add(Long.parseLong(ids));
                if (reply == null) {
                    Message message = new Message(from1,list_to_ids,message1);
                    message.setId(id);
                    message.setData(date1.toLocalDateTime());
                    return message;

                } else {
                    Message replyMessage = findOne(reply);
                    Message message = new Message(from1,list_to_ids,message1,replyMessage);
                    message.setId(id);
                    message.setData(date1.toLocalDateTime());
                    return message;
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Message> findAll() {
        Set<Message> messages = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Messages"+'"');
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long from1 = resultSet.getLong("from");
                String to1 = resultSet.getString("to");
                String message1= resultSet.getString("message");
                Timestamp date1=resultSet.getTimestamp("date");
                Long reply=resultSet.getLong("idReply");

                List<String> list_to_String= Arrays.asList(to1.split(","));

                List<Long>list_to_ids=new ArrayList<>();
                for(String tos:list_to_String)
                    list_to_ids.add(Long.parseLong(tos));

                if(reply==null) {
                    Message message = new Message(from1,list_to_ids,message1);
                    message.setId(id);
                    message.setData(date1.toLocalDateTime());
                    messages.add(message);
                }
                else{
                    Message replyMessage=findOne(reply);
                    Message message = new Message(from1,list_to_ids,message1,replyMessage);
                    message.setId(id);
                    message.setData(date1.toLocalDateTime());
                    messages.add(message);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }

    @Override
    public Message save(Message entity) {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);

        //nu inserez id-e autoincrement
        if(entity.getReply()==null)
        { try (Connection connection = DriverManager.getConnection(url, username, password);
               PreparedStatement statement = connection.prepareStatement("INSERT INTO public."+'"'+"Messages"+'"'+"("+'"'+"from"+'"'+","+'"'+"to"+'"'+","+'"'+"date"+'"'+","+'"'+"message"+'"'+")"+" VALUES (?,?,?,?)"))
        {
            statement.setLong(1,entity.getId_from());
            statement.setString(2,String.valueOf(entity.getToAsString()));
            statement.setString(4,String.valueOf(entity.getMesaj()));//4 sau 5
            statement.setTimestamp(3,Timestamp.valueOf(entity.getData()));
            try {
                ResultSet resultSet = statement.executeQuery();
                statement.executeUpdate();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    else{
    //reply nu e null
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement statement = connection.prepareStatement("INSERT INTO public."+'"'+"Messages"+'"'+"("+'"'+"from"+'"'+","+'"'+"to"+'"'+","+'"'+"date"+'"'+","+'"'+"idReply"+'"'+","+'"'+"message"+'"'+")"+" VALUES (?,?,?,?,?)"))
            {
                statement.setLong(1,entity.getId_from());
                statement.setString(2,String.valueOf(entity.getToAsString()));
                statement.setString(5,String.valueOf(entity.getMesaj()));
                statement.setTimestamp(3,Timestamp.valueOf(entity.getData()));
                statement.setLong(4,entity.getReply().getId());
                try {
                    ResultSet resultSet = statement.executeQuery();
                    statement.executeUpdate();
                }
                catch (PSQLException e){};
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
    }
    }

    @Override
    public Message delete(Long aLong) {
        return null;
    }

    @Override
    public Message update(Message entity) {
        return null;
    }

}
