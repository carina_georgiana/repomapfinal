package socialnetwork.repository.database;

import org.postgresql.util.PSQLException;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageImplementation;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PrieteniiDB_Repo implements PagingRepository<Tuple<Long,Long>, Prietenie> {
    private String url;
    private String username;
    private String password;
    private Validator<Prietenie> validator;

    public PrieteniiDB_Repo(String url, String username, String password, Validator<Prietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public Page<Prietenie> findAll(Pageable pageable) {
        List<Prietenie> prietenii=new ArrayList<>();
        try(Connection connection=DriverManager.getConnection(url,username,password);
            PreparedStatement statement=connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"'+" ORDER BY id LIMIT "+pageable.getPageSize()+" OFFSET "+pageable.getPageSize()*pageable.getPageNumber());
            ResultSet resultSet=statement.executeQuery()){
            while(resultSet.next()){
                Long id1=resultSet.getLong("id1");
                Long id2=resultSet.getLong("id2");
               Timestamp data=resultSet.getTimestamp("date");
                String status=resultSet.getString("status");
                Prietenie prietenie=new Prietenie(id1,id2,data.toLocalDateTime(),status);
                prietenie.setId(new Tuple<>(id1,id2));
                prietenii.add(prietenie);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return new PageImplementation<>(pageable,prietenii.stream());
    }

    @Override
    public Prietenie findOne(Tuple<Long, Long> tuple) {
        if(tuple==null)
        {
            System.out.println("id null prietenie");
            return null;
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"'+" WHERE id1=? and id2=?"))
        {
            statement.setLong(1,tuple.getLeft());
            statement.setLong(2,tuple.getRight());
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                Timestamp date1=resultSet.getTimestamp("date");
                LocalDateTime date=date1.toLocalDateTime();
                String status=resultSet.getString("status");//numele coloanelor in fct de get
                Prietenie p = new Prietenie(id1,id2,date,status);
                Tuple x = new Tuple(id1, id2);
                p.setId(x);
                return p;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Prietenie> findAll() {
        Set<Prietenie> friends = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"');
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String status=resultSet.getString("status");
                Timestamp date1=resultSet.getTimestamp("date");
                LocalDateTime date=date1.toLocalDateTime();
                Prietenie p=new Prietenie(id1,id2,date,status);
                Tuple x=new Tuple(id1,id2);
                p.setId(x);
                p.setStatus(status);
                friends.add(p);
            }
            return friends;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friends;
    }

    @Override
    public Prietenie save(Prietenie entity) {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
        PreparedStatement statement = connection.prepareStatement("INSERT INTO public."+'"'+"Prietenii"+'"'+"("+'"'+"id1"+'"'+","+'"'+"id2"+'"'+","+'"'+"date"+'"'+","+'"'+"status"+'"'+")"+" VALUES (?,?,?,?)"))
        {
            statement.setLong(1,entity.getId().getLeft());
            statement.setLong(2,entity.getId().getRight());
            statement.setString(4,String.valueOf(entity.getStatus()));
            statement.setTimestamp(3,Timestamp.valueOf(entity.getDate()));
            try {
                ResultSet resultSet = statement.executeQuery();
                statement.executeUpdate();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Prietenie delete(Tuple<Long, Long> tuple) {
        if(tuple==null)
            throw new IllegalArgumentException("id must be not null");
        Prietenie p=findOne(tuple);
        if(p==null)
           return p;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM public."+'"'+"Prietenii"+'"'+" WHERE id1=? and id2=?"))
        {
            statement.setLong(1,tuple.getLeft());
            statement.setLong(2,tuple.getRight());
            try {
                ResultSet resultSet = statement.executeQuery();
                statement.executeUpdate();
            }catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return p;
    }


    @Override
    public Prietenie update(Prietenie entity) {
        if(entity==null)
            throw new IllegalArgumentException();
        validator.validate(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("UPDATE public."+'"'+"Prietenii"+'"'+" SET status=? WHERE id1=? and id2=?"))
        {
            statement.setString(1,String.valueOf(entity.getStatus()));
            statement.setLong(2,entity.getId().getLeft());
            statement.setLong(3,entity.getId().getRight());
            try {
                ResultSet resultSet = statement.executeQuery();
                statement.executeUpdate();
                return null;
            }catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
