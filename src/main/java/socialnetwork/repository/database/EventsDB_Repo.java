package socialnetwork.repository.database;

import org.postgresql.util.PSQLException;
import socialnetwork.BCrypt.MyCrypt;
import socialnetwork.domain.Event;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageImplementation;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDate;
import java.util.*;

public class EventsDB_Repo implements PagingRepository<Long, Event> {
    private String url;
    private String username;
    private String password;
    private Validator<Event> validator;
    public EventsDB_Repo(String url, String username, String password, Validator<Event> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public Event findOne(Long aLong) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events"+'"'+" WHERE id=?"))
        {
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long id_user = resultSet.getLong("id_user");
                String titlu = resultSet.getString("titlu");
                String descriere = resultSet.getString("descriere");
                Timestamp date1 = resultSet.getTimestamp("date");
                LocalDate data=date1.toLocalDateTime().toLocalDate();
                Event e=new Event(id,id_user,titlu,descriere,data);
                return e;

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Event> findAll() {
        Set<Event> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events"+'"');
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long id_user = resultSet.getLong("id_user");
                String titlu = resultSet.getString("titlu");
                String descriere = resultSet.getString("descriere");
                Timestamp date1 = resultSet.getTimestamp("date");
                LocalDate data=date1.toLocalDateTime().toLocalDate();


                Event ev=new Event(id,id_user,titlu,descriere,data);
                events.add(ev);


            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }

    @Override
    public Event save(Event entity) {
        //aici user vine fara id,aici setez id ul
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO public.\"Events\"("+
                     "\"date\", \"titlu\", \"descriere\",\"id_user\")"+
                     "VALUES (?, ?, ?,?);"))
        {
            statement.setTimestamp(1,Timestamp.valueOf(entity.getDate().atStartOfDay()));
            statement.setString(2,entity.getTitlu());
            statement.setString(3,entity.getDescriere());
            statement.setLong(4,entity.getId_user());
            try {
                ResultSet resultSet = statement.executeQuery();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Event delete(Long aLong) {
        if(aLong==null)
            throw new IllegalArgumentException("id must be not null");
        Event u=findOne(aLong);
        if(u==null)
            return u;
        //pt tabel m-n
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM public."+'"'+"Events_Users"+'"'+" WHERE id_event=?"))
        {
            statement.setLong(1,aLong);
            try {

                ResultSet resultSet = statement.executeQuery();
                statement.executeUpdate();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }


        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM public."+'"'+"Events"+'"'+" WHERE id=?"))
        {
            statement.setLong(1,aLong);
            try {

                ResultSet resultSet = statement.executeQuery();
                statement.executeUpdate();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return u;
    }

    @Override
    public Event update(Event entity) {
        return null;
    }

    //abonare un user la un event-trebuie validat input,altfel=>crapa la foreign key in loc sa arunce exceptie
    public boolean abonare(Long id_user,Long id_event) {
        if (id_event==null||id_user==null)
            throw new IllegalArgumentException("entity must be not null");
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO public."+'"'+"Events_Users"+'"'+"("+'"'+"id_user"+'"'+","+'"'+"id_event"+'"'+")" +"VALUES (?,?)"))
        {
            statement.setLong(1,id_user);
            statement.setLong(2,id_event);
            try {
                ResultSet resultSet = statement.executeQuery();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
        //mereu va da return true
    }

    //dezabonare user de la event
    public boolean dezabonare(Long id_user,Long id_event) {
        if (id_event==null||id_user==null)
            throw new IllegalArgumentException("entity must be not null");
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM public."+'"'+"Events_Users"+'"'+" WHERE id_user=? and id_event=?"))
        {
            statement.setLong(1,id_user);
            statement.setLong(2,id_event);
            try {
                ResultSet resultSet = statement.executeQuery();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
        //mereu va da return true
    }
    //toate evenimentele la care s a abonat userul x
    //toate events create de x
    public Iterable<Event> abonat_la(Long aLong) {
        //daca id null=>exceptie
        Set<Event> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events_Users"+'"'+" WHERE id_user=?"))
        {
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("id_event");
                //construiesc e
                Event e=findOne(id);
                events.add(e);

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }
    //events create de x
    public Iterable<Event> create_de(Long aLong) {
        //daca id null=>exceptie
        Set<Event> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Events"+'"'+" WHERE id_user=?"))
        {
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long id_user = resultSet.getLong("id_user");
                String titlu = resultSet.getString("titlu");
                String descriere = resultSet.getString("descriere");
                Timestamp date1 = resultSet.getTimestamp("date");
                LocalDate data=date1.toLocalDateTime().toLocalDate();
                Event e=new Event(id,id_user,titlu,descriere,data);
                events.add(e);

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }

    @Override
    public Page<Event> findAll(Pageable pageable) {
        List<Event>evenim=new ArrayList<>();
        try(Connection connection=DriverManager.getConnection(url,username,password);
            PreparedStatement statement=connection.prepareStatement("SELECT * from public."+'"'+"Events"+'"'+" ORDER BY date LIMIT "+pageable.getPageSize()+" OFFSET "+pageable.getPageSize()*pageable.getPageNumber());
            ResultSet resultSet=statement.executeQuery()){
            while(resultSet.next()){
                Long id=resultSet.getLong("id");
                String titlu=resultSet.getString("titlu");
                String descriere=resultSet.getString("descriere");
                Timestamp data=resultSet.getTimestamp("date");
                Long id_user=resultSet.getLong("id_user");

                LocalDate date=data.toLocalDateTime().toLocalDate();

                Event ev=new Event(id,id_user,titlu,descriere,date);
                ev.setId(id);
                evenim.add(ev);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return new PageImplementation<>(pageable,evenim.stream());
    }

}
