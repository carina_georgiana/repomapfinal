package socialnetwork.repository.database;

import org.postgresql.util.PSQLException;
import socialnetwork.BCrypt.MyCrypt;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;

import java.sql.*;
import java.util.*;

public class UtilizatorDB_Repo implements PagingRepository<Long, Utilizator> {
    private String url;
    private String username;
    private String password;
    private Validator<Utilizator> validator;

    public UtilizatorDB_Repo(String url, String username, String password, Validator<Utilizator> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public Utilizator findOne(Long aLong) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Utilizatori"+'"'+" WHERE id=?"))
        {
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String email=resultSet.getString("email");
                String password=resultSet.getString("password");
                //Utilizator utilizator = new Utilizator(firstName, lastName,email,"eusunt"+id);//aici are id=null
                Utilizator utilizator = new Utilizator(firstName, lastName,email,password);
                utilizator.setId(id);
                return utilizator;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Utilizator> findAll() {
        Set<Utilizator> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Utilizatori"+'"');
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String email=resultSet.getString("email");
                String password=resultSet.getString("password");
               // Utilizator utilizator = new Utilizator(firstName, lastName,email,"eusunt"+id);
                Utilizator utilizator = new Utilizator(firstName, lastName,email,password);
                utilizator.setId(id);

                for(Prietenie p:findAllFriendships()) {
                    if (p.getStatus() != null) {
                        if (p.getStatus().equals("accepted")) {
                            if (p.getId().getRight().equals(id)) {
                                utilizator.addPrieten(findOne(p.getId().getLeft()));
                            } else {
                                if ((p.getId().getLeft().equals(id))) {
                                    utilizator.addPrieten(findOne(p.getId().getRight()));
                                }
                            }
                        }
                    }
                }
                users.add(utilizator);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public Utilizator save(Utilizator entity) {
        //aici user vine fara id,aici setez id ul
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             //"SELECT * from public."+'"'+"Utilizatori"+'"'+" WHERE id=?"
             //PreparedStatement statement = connection.prepareStatement("INSERT INTO public."+'"'+"Utilizatori"+'"'+"("+'"'+"firstName"+'"'+","+'"'+"lastName"+'"'+","+'"'+"email"+'"'+'"'+"password"+'"'+")" +"VALUES (?,?,?,?)"))
             PreparedStatement statement = connection.prepareStatement("INSERT INTO public.\"Utilizatori\"("+
                     "\"firstName\", \"lastName\", \"email\",\"password\")"+
                     "VALUES (?, ?, ?,?);"))
        {
            statement.setString(1,entity.getFirstName());
            statement.setString(2,entity.getLastName());
            statement.setString(3,entity.getEmail());
            //pun hash ul in bd,nu parola
           // statement.setString(4, MyCrypt.hash(entity.getPassword()));
            statement.setString(4,entity.getPassword());

            try {
                ResultSet resultSet = statement.executeQuery();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Utilizator delete(Long aLong) {
        if(aLong==null)
            throw new IllegalArgumentException("id must be not null");
        Utilizator u=findOne(aLong);
        if(u==null)
            return u;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM public."+'"'+"Utilizatori"+'"'+" WHERE id=?"))
        {
            statement.setLong(1,aLong);
            try {

                ResultSet resultSet = statement.executeQuery();
                statement.executeUpdate();
            }
            catch (PSQLException e){};
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return u;
    }

    @Override
    public Utilizator update(Utilizator entity) {
        return null;
    }

    public Iterable<Prietenie> findAllFriendships() {
        Set<Prietenie> friends = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from public."+'"'+"Prietenii"+'"');
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String status=resultSet.getString("status");
                Timestamp data=resultSet.getTimestamp("date");
                Prietenie p=new Prietenie();
                Tuple x=new Tuple(id1,id2);
                p.setId(x);
                p.setStatus(status);
                p.setDate(data.toLocalDateTime());
                friends.add(p);
            }
            return friends;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friends;
    }

    public Iterable<Utilizator>reloadFriendships(){
        Set<Utilizator> users = new HashSet<>();
        for(Utilizator u:findAll()){
            Long id=u.getId();
            for(Prietenie p:findAllFriendships()){
                if(p.getId().getLeft().equals(id))
                    u.addPrieten(findOne(p.getId().getRight()));
                else
                if(p.getId().getRight().equals(id))
                    u.addPrieten(findOne(p.getId().getLeft()));
            }
            users.add(u);
        }
        return users;
    }


    @Override
    public Page<Utilizator> findAll(Pageable pageable) {
        //Paginator<Utilizator> paginator = new Paginator<Utilizator>(pageable, this.findAll());
        //return paginator.paginate();
        List<Utilizator>users=new ArrayList<>();
        try(Connection connection=DriverManager.getConnection(url,username,password);
        PreparedStatement statement=connection.prepareStatement("SELECT * from public."+'"'+"Utilizatori"+'"'+" ORDER BY id LIMIT "+pageable.getPageSize()+" OFFSET "+pageable.getPageSize()*pageable.getPageNumber());
        ResultSet resultSet=statement.executeQuery()){
            while(resultSet.next()){
                Long id=resultSet.getLong("id");
                String firstName=resultSet.getString("firstName");
                String lastName=resultSet.getString("lastName");
                String email=resultSet.getString("email");
                String password=resultSet.getString("password");
                Utilizator utilizator=new Utilizator(firstName,lastName,email,password);
                utilizator.setId(id);
                users.add(utilizator);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return new PageImplementation<>(pageable,users.stream());
    }


    /*public Iterable<Utilizator> findPage(int page) {
        //return userii de pe o pagina
        Set<Utilizator> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);

             // page 1 - 0-0+6
             // page 2 - 7-7+6

             PreparedStatement statement =
                     connection.prepareStatement("SELECT * from public."+'"'+"Utilizatori"+'"'+" LIMIT 5 OFFSET 5*"+page);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String email=resultSet.getString("email");
                //String pass1=resultSet.getString("password");--ar trebui decodata!!!!!!!!!!

                Utilizator utilizator = new Utilizator(firstName, lastName,email,"eusunt"+id);
                utilizator.setId(id);

                for(Prietenie p:findAllFriendships()) {
                    if (p.getStatus() != null) {
                        if (p.getStatus().equals("accepted")) {
                            if (p.getId().getRight().equals(id)) {
                                utilizator.addPrieten(findOne(p.getId().getLeft()));
                            } else {
                                if ((p.getId().getLeft().equals(id))) {
                                    utilizator.addPrieten(findOne(p.getId().getRight()));
                                }
                            }
                        }
                    }
                }
                users.add(utilizator);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }*/

}




