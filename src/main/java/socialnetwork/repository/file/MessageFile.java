package socialnetwork.repository.file;

import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageFile extends AbstractFileRepository<Long, Message>{
    //ex format mesaj:1;1;[2,3];mesaj;data;2
    public Repository<Long, Utilizator> repoUtilizatori;
    //public Repository<Long,Utilizator> getRepoUtilizatori(){return repoUtilizatori;}
    //sau sa fac de tip abstractfilerepo,ca sa nu lucrez cu obiecte de tip interfata
    public MessageFile(String fileName, Validator<Message> validator,Repository<Long,Utilizator>repoUtilizatori) {
        super(fileName, validator);
        //in mesaj iau lista de iduri,in extract entity to =null,in constructor mesaj imi pun lista de isuri
        this.repoUtilizatori=repoUtilizatori;
        //per mesaj acum am:id_from,lista<long>to,reply,mesaj,data,id,mai trebuie:from ca user si list<useri>toUseri
        //aici o fct care pune cele de mai sus pt fiecare mesaj

    }

    @Override
    public Message extractEntity(List<String> attributes) {
        //0:id,1:from,2:lista id uri to,3-mesaj,4-data,5-id reply,eventual null
        List<Long>iduri=new ArrayList<>();
        String a1=attributes.get(2).replace("[","");
        String a2=a1.replace("]","");
        //obs,la split e posibil sa trebuiasca un try catch pt NumberFormatException
        List<String>ids_string= Arrays.asList(a2.split(", "));

        ids_string.forEach(s->iduri.add(Long.valueOf(s)));//id uri e lista pt toIduri

       // List<Utilizator>to=new ArrayList<>();
        //for(Long id:iduri){
          //  Utilizator u1=repoUtilizatori.findOne(id);
            //to.add(u1);
        //}

        Long id_from=Long.parseLong(attributes.get(1));
        //Utilizator from=repoUtilizatori.findOne(id_from);

        Message m= new Message(id_from,iduri,attributes.get(3));
        m.setId(Long.parseLong(attributes.get(0)));
        m.setData(LocalDateTime.parse(attributes.get(4)));
        if(attributes.get(5).equals("null"))
            m.setReply(null);
        else{
            Long id_reply=Long.parseLong(attributes.get(5));
            Message reply=findOne(id_reply);
            m.setReply(reply);
        }
        return m;
    }

    @Override
    protected String createEntityAsString(Message entity) {
        List<Long>iduri=entity.getTo();
        //entity.getTo().forEach(x->iduri.add(x));
        if(entity.getReply()==null)
            return entity.getId()+";"+entity.getId_from()+";"+iduri.toString()+";"+entity.getMesaj()+";"+entity.getData()+";"+"null";
        else
        return entity.getId()+";"+entity.getId_from()+";"+iduri.toString()+";"+entity.getMesaj()+";"+entity.getData()+";"+entity.getReply().getId();
    }
}
