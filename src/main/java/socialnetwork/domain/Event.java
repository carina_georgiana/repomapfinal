package socialnetwork.domain;

import java.time.LocalDate;

public class Event extends Entity<Long>{
    private Long id_user;
    private String titlu;
    private String descriere;
    private LocalDate date;
    public Event(Long id,Long id_user,String titlu,String descriere,LocalDate date){
        this.titlu=titlu;
        this.descriere=descriere;
        this.id_user=id_user;
        this.date=date;
        this.setId(id);
    }
    public Event(Long id_user,String titlu,String descriere,LocalDate date){
        this.titlu=titlu;
        this.descriere=descriere;
        this.id_user=id_user;
        this.date=date;
    }

    public Long getId_user() {
        return this.id_user;
    }
    public void setId_user(Long id){this.id_user=id;}
    public String getTitlu(){return this.titlu;}
    public String getDescriere(){return this.descriere;}
    public void setTitlu(String t){this.titlu=t;}
    public void setDescriere(String t){this.descriere=t;}
    public LocalDate getDate(){return this.date;}
    public void setDate(LocalDate date){this.date=date;}

    @Override
    public String toString() {
        return "titlu="+titlu+",desc="+descriere+",data="+date.toString();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Event that = (Event) o;
        return getTitlu().equals(that.getTitlu()) &&
                getDate().equals(that.getDate()) &&
                getDescriere().equals(that.getDescriere());
    }
}
