package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;
    private String status;
    public String getStatus(){return status;}
    public void setStatus(String status){this.status=status;}

    public Prietenie(){
        this.setDate(LocalDateTime.now());
        this.status="pending";}
    public Prietenie(Long id1,Long id2,LocalDateTime date) {
        Tuple<Long,Long>t=new Tuple<>(id1,id2);
        this.setId(t);
        this.setStatus("pending");
        this.date=date;
    }
    public Prietenie(Long id1,Long id2,LocalDateTime date,String status) {
        Tuple<Long,Long>t=new Tuple<>(id1,id2);
        this.setId(t);
        this.setStatus(status);
        this.date=date;
    }
    public int getMonth(){
        String dataString=this.getDate().toString();
        List<String> data= Arrays.asList(dataString.split("-"));
        String luna=data.get(1);
        int rez=Integer.parseInt(luna);
        return rez;
    }
    public int getAn(){
        String dataString=this.getDate().toString();
        List<String> data= Arrays.asList(dataString.split("-"));
        String an=data.get(0);
        int rez=Integer.parseInt(an);
        return rez;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId().getLeft(),this.getId().getRight());
    }

    @Override
    public boolean equals(Object obj) {
        if(this==obj)
            return true;
        if(obj==null)
            return false;
        if(obj.getClass()!=this.getClass())
            return false;
        Prietenie nou=(Prietenie)obj;
        return Objects.equals(this.getId().getLeft(),nou.getId().getLeft())&&Objects.equals(this.getId().getRight(),nou.getId().getRight());
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime date){this.date=date;}
    @Override
    public String toString(){
        return "Prietenie{"+this.getId().toString()+","+this.getDate()+","+this.getStatus()+"}";
    }
}
