package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;
//nu l mai folosesc
public class RepoValidator implements Validator<Utilizator>{

    @Override
    public void validate(Utilizator entity) throws ValidationException {
        String errors="";
        if(entity.getFirstName().length()<3||entity.getFirstName().length()>15)
            errors+="primul nume are intre 3 si 15 litere\n";
        if(entity.getLastName().length()<3||entity.getLastName().length()>15)
            errors+="al doilea  nume are intre 3 si 15 litere\n";
        if(!entity.getFirstName().chars().allMatch(Character::isLetter)||!entity.getLastName().chars().allMatch(Character::isLetter))
            errors+="numele contin doar litere\n";
        if(errors.length()>0)
            throw new ValidationException(errors);
    }
}
