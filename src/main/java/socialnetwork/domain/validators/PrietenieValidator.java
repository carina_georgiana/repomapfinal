package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;

public class PrietenieValidator implements Validator<Prietenie> {
    @Override
    public void validate(Prietenie entity) throws ValidationException {
        String errors=new String();//sau ="";

        if(entity.getId().getRight()==entity.getId().getLeft())
            errors+="prietenia se realizeaza intre 2 utilizatori distincti\n";
        if(errors.length()>0)
            throw new ValidationException(errors);
    }
}
