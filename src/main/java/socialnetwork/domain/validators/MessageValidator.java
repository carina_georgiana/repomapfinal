package socialnetwork.domain.validators;

import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;

public class MessageValidator implements Validator<Message> {
    @Override
    public void validate(Message entity) throws ValidationException {
        String errors="";
        if(entity.getMesaj().equals(""))
            errors+="mesajul nu poate fi vid";
        if(errors.length()>0)
            throw new ValidationException(errors);
    }

}
