package socialnetwork.domain.validators;

import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;

import java.time.LocalDate;

public class EventValidator implements Validator<Event> {
    @Override
    public void validate(Event entity) throws ValidationException {
        String errors="";
        if(entity.getTitlu().isEmpty())
            errors+="titlul nu poate fi vid\n";
        if(entity.getTitlu().length()>25)
            errors+="titlu prea lung\n";
        if(entity.getDescriere().isEmpty())
            errors+="descrierea nu poate fi vida\n";
        if(entity.getDate().isBefore(LocalDate.now()))
            errors+="evenimentul a trecut deja\n";
        if(errors.length()>0)
            throw new ValidationException(errors);
    }

}
