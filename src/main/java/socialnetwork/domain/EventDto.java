package socialnetwork.domain;

import java.time.LocalDate;

public class EventDto extends Event{
    int nr_zile;
    public int getNr_zile(){return this.nr_zile;}
    public void setNr_zile(int nr){this.nr_zile=nr;}
    public EventDto(Long id, Long id_user, String titlu, String descriere, LocalDate date,int nr) {
        super(id, id_user, titlu, descriere, date);
        this.nr_zile=nr;
    }

    public EventDto(Long id_user, String titlu, String descriere, LocalDate date,int nr_zile) {
        super(id_user, titlu, descriere, date);
        this.nr_zile=nr_zile;
    }
    @Override
    public String toString(){
        return "mai sunt "+nr_zile+" zile pana la evenimentul: "+getTitlu().toUpperCase();
    }
}
