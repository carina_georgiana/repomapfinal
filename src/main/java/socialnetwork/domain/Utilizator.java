package socialnetwork.domain;

import socialnetwork.domain.validators.ValidationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Utilizator extends Entity<Long>{
    private String firstName;
    private String lastName;
    private String email;
    private List<Utilizator> friends;
    private String password;

    public Utilizator(String firstName, String lastName,String email,String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email=email;
        friends=new ArrayList<Utilizator>();
        this.password=password;
        //this.setId((long)this.hashCode());
    }
    public Utilizator(Long id,String firstName, String lastName,String email,String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email=email;
        friends=new ArrayList<Utilizator>();
        this.password=password;
        this.setId(id);
    }
    public boolean suntPrieteni(Utilizator utilizator){
        for(Utilizator u:getFriends())
            if(utilizator.getId().equals(u.getId()))
                return true;
        return false;
    }
    public String getEmail(){return email;}
    public void setEmail(String email){this.email=email;}
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Utilizator> getFriends() {
        return friends;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString(){
        return "firstName="+firstName+", lastName="+lastName+", email="+email;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getFriends().equals(that.getFriends());
    }
    public void addPrieten(Utilizator u){
        if(u!=null && !friends.contains(u))
            friends.add(u);
    }
    public void removePrieten(Utilizator u) {
        if(u!=null&&friends.contains(u))
            friends.remove(u);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends(),getEmail());
    }
    public void setFriends(List<Utilizator> friends) {
        this.friends = friends;
    }

}