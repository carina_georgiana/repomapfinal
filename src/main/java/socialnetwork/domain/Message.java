package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> {
    String mesaj;
    LocalDateTime data;
    Message reply;

    private Long id_from;
    private List<Long>to;
   public Message(Long from,List<Long>to,String mesaj){
        this.id_from=from;
        this.to=to;
        this.data=LocalDateTime.now();
        this.mesaj=mesaj;
        this.reply=null;

    }
    public Message(Long from,List<Long>to,String mesaj,Message reply){
        this.id_from=from;
        this.to=to;
        this.data=LocalDateTime.now();//si id
        this.mesaj=mesaj;
        this.reply=reply;
   }

    public void setFromId(Long id){
        this.id_from=id;
    }
    public Long getId_from(){return this.id_from;}

    public List<Long>getTo(){
        return this.to;
    }
    public void setTo(List<Long>to){this.to=to;}

    public String getMesaj() {
        return mesaj;
    }

    public void setMesaj(String mesaj) {
        this.mesaj = mesaj;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Message getReply() {
        return reply;
    }

    public void setReply(Message reply) {
        this.reply = reply;
    }
    @Override
    public String toString(){
        if(reply==null)
            return "from="+id_from+", to="+to+", mesaj="+mesaj;
        else
            return "from="+id_from+", to="+to+", mesaj="+mesaj+", reply="+reply.getMesaj();
    }
    public String toStringReply(){
        if(reply==null)
            return "reply null";
        else{
            return reply.getId()+" "+reply.getMesaj();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return to.equals(message.to) &&
                mesaj.equals(message.mesaj) &&
                Objects.equals(data, message.data);
                //&&Objects.equals(reply, message.reply);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_from, to, mesaj, data, reply);
    }

    public String getToAsString() {
        String lista="";
        for(Long i:to)
        {   //elem sunt distincte
            if(!i.equals(to.get(to.size()-1)))
        {
            String is=i.toString();
            lista=lista+is+",";}
            else{
                String is1=i.toString();
                lista=lista+is1;
        }
        }
        return lista;
    }


}
